package com.robotico.powershell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.robotico.lib.MainUtil;
import com.robotico.listener.ExtentScreenCapture;
import com.robotico.listener.ExtentTestNGITestListener;

public class PowerShellBasicCommands {

	public String getSystemInfo() {
		Path dst = null;
		try {
			ExtentTestNGITestListener.createNode("SYSTEM INFOMRATION");
			String command = "Get-ComputerInfo | fl";
			System.out.println(command);
			Process powerShellProcess = executeCommandWithoutOutPut(command, "SYSTEM_INFO", "systemInfo", "N");
			System.out.println("");
			String line;
			BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
			BufferedWriter writer;
			dst = Paths.get(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + "SYSTEM_INFO.txt");

			writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8);
			if (stdout.readLine() == null) {
				System.out.println("File is empty");
				MainUtil.LogFail("SYSTEM_INFO" + " not found", "systemInfo", null);
				stdout.close();
				writer.close();
			} else {

				System.out.println("Standard Output:");
				while ((line = stdout.readLine()) != null) {
					if (!line.equalsIgnoreCase("")) {
						writer.write(line);
						writer.newLine();
					}
				}
				stdout.close();
				writer.close();
				ExtentScreenCapture.AttachTextFile("SYSTEM_INFO");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dst.toString();
	}

	public Process executeCommandWithoutOutPut(String command, String comment, String AppName, String SS) {
		Process powerShellProcess = null;
		try {
			command = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe " + command;
			System.out.println(command);

			powerShellProcess = Runtime.getRuntime().exec(command);

			if ((SS != null || !SS.equalsIgnoreCase("")) && SS.equalsIgnoreCase("Y")) {
				Thread.sleep(10000);
				BufferedReader stderr = null;
				stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
				if (stderr.readLine() != null) {
					System.out.println("Standard Error:");

					StringBuilder strErrorbuilder = new StringBuilder();
					String errorLine;
					while ((errorLine = stderr.readLine()) != null) {
						strErrorbuilder.append(errorLine);
					}

					MainUtil.LogInfo(strErrorbuilder == null ? "" : strErrorbuilder.toString(), "Error reason");
					MainUtil.LogFail("error occured running the command", AppName);
				} else {
					System.out.println("Command ran successfully");
					MainUtil.LogPass(comment, AppName);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		}
		return powerShellProcess;
	}

	public void executePS1File(String command, String textFileName, String appName, String... fileYN) {
		try {
			if (fileYN.length > 0 && fileYN[0].equalsIgnoreCase("N"))
				executeCommand("-ExecutionPolicy ByPass  " + command, textFileName, appName);
			else
				executeCommand("-ExecutionPolicy ByPass -File " + command, textFileName, appName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void executePS1FileWithoutOutPut(String command, String textFileName, String AppName, String... fileYN) {
		BufferedReader stderr = null;
		BufferedWriter writer = null;
		try {
			Process powerShellProcess;
			if (fileYN.length > 0 && fileYN[0].equalsIgnoreCase("N"))
				powerShellProcess = executeCommandWithoutOutPut("-ExecutionPolicy ByPass  " + command, textFileName,
						AppName, "Y");
			else
				powerShellProcess = executeCommandWithoutOutPut("-ExecutionPolicy ByPass -File " + command,
						textFileName, AppName, "Y");

			Path dst = Paths.get(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + AppName + ".txt");

			writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8);

			stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
			if (stderr.readLine() == null) {
				Thread.sleep(4000);
				MainUtil.LogPass("Ps1 File ran successfully", AppName);
				System.out.println("There is no error ");
				stderr.close();
				writer.close();
			} else {
				System.out.println("Standard Error:");

				StringBuilder strErrorbuilder = new StringBuilder();
				String errorLine;
				while ((errorLine = stderr.readLine()) != null) {
					strErrorbuilder.append(errorLine);
				}

				MainUtil.LogInfo(strErrorbuilder == null ? "" : strErrorbuilder.toString(), "Error reason");
				MainUtil.LogFail("error occured running the command", AppName);
				ExtentScreenCapture.AttachTextFile(AppName);

				/*
				 * System.out.println("Standard Error:"); String line; while ((line =
				 * stderr.readLine()) != null) { System.out.println(line); writer.write(line);
				 * writer.newLine(); }
				 * 
				 * MainUtil.LogFail("error", AppName);
				 * ExtentScreenCapture.AttachTextFile(AppName);
				 */

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stderr.close();
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public void executeCommand(String command, String textFileName, String appName) throws Exception {
		try {
			textFileName = textFileName.replaceAll(" ", "_");
			command = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe " + command;
			System.out.println(command);

			Process powerShellProcess = Runtime.getRuntime().exec(command);
			System.out.println("");
			String line;
			BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
			BufferedWriter writer;
			Path dst = Paths
					.get(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + textFileName + ".txt");

			writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8);
			if (stdout.readLine() == null) {
				System.out.println("File is empty");
				MainUtil.LogFail(textFileName + " not found", appName);
				stdout.close();
				writer.close();
			} else {

				System.out.println("Standard Output:");
				while ((line = stdout.readLine()) != null) {
					if (!line.equalsIgnoreCase("")) {
						System.out.println(line);
						writer.write(line);
						writer.newLine();
					}
				}
				stdout.close();
				writer.close();
				MainUtil.LogPass(textFileName + " info found", appName);
				ExtentScreenCapture.AttachTextFile(textFileName);
			}

			BufferedReader stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
			if (stderr.readLine() == null) {
				System.out.println("There is no error ");
				stderr.close();
				writer.close();
			} else {
				System.out.println("Standard Error:");

				StringBuilder strErrorbuilder = new StringBuilder();
				String errorLine;
				while ((errorLine = stderr.readLine()) != null) {
					strErrorbuilder.append(errorLine);
				}

				MainUtil.LogInfo(strErrorbuilder == null ? "" : strErrorbuilder.toString(), "Error reason");
				MainUtil.LogFail(textFileName + " - error occured ", appName);
				ExtentScreenCapture.AttachTextFile(textFileName);

				/*
				 * System.out.println("Standard Error:"); while ((line = stderr.readLine()) !=
				 * null) { System.out.println(line); writer.write(line); writer.newLine(); }
				 * stderr.close(); writer.close(); MainUtil.LogFail(textFileName + " - error",
				 * appName); ExtentScreenCapture.AttachTextFile(textFileName);
				 */
			}
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		}

	}

	public void executeCommandword(String command, String wordFileName, String appName, String sleep) throws Exception {
		try {
			wordFileName = wordFileName.replaceAll(" ", "_");
			command = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe " + command;
			System.out.println(command);

			Process powerShellProcess = Runtime.getRuntime().exec(command);

			Thread.sleep(Long.parseLong(sleep));
			System.out.println("");
			String line;
			BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
			BufferedWriter writer;
			Path dst = Paths
					.get(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + wordFileName + ".docx");

			XWPFDocument document = new XWPFDocument();
			FileOutputStream out = new FileOutputStream(new File(
					ExtentTestNGITestListener.passLocation + MainUtil.file_separator + wordFileName + ".docx"));
			XWPFParagraph n = document.createParagraph();
			XWPFRun run = n.createRun();

			if (stdout.readLine() == null) {
				System.out.println("File is empty");
				MainUtil.LogFail(wordFileName + " not found", appName);
				stdout.close();
			} else {

				System.out.println("Standard Output:");
				while ((line = stdout.readLine()) != null) {
					run.addCarriageReturn();
					run.setText(line);
				}
				document.write(out);
				document.close();
				out.close();
				MainUtil.LogPass(wordFileName + " info found", appName);
				ExtentScreenCapture.AttachTextFile(wordFileName);
			}

			writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8);

			BufferedReader stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
			if (stderr.readLine() == null) {
				System.out.println("There is no error ");
				stderr.close();
				writer.close();
			} else {
				System.out.println("Standard Error:");

				StringBuilder strErrorbuilder = new StringBuilder();
				String errorLine;
				while ((errorLine = stderr.readLine()) != null) {
					strErrorbuilder.append(errorLine);
				}

				MainUtil.LogInfo(strErrorbuilder == null ? "" : strErrorbuilder.toString(), "Error reason");
				MainUtil.LogFail(wordFileName + " - error occured ", appName);
				ExtentScreenCapture.AttachTextFile(wordFileName);

			}
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		}

	}

	public static void captureScreenshot(String destination) {
		Process powerShellProcess = null;
		try {
			ClassLoader classLoader = ExtentScreenCapture.class.getClassLoader();

			String command = "(" + MainUtil.storeVariable.get("FILE_PATH_LOCATION")
					+ "\\PS_Files\\captureScreenshot.ps1" + " '" + destination + "')";
			command = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -ExecutionPolicy ByPass "
					+ command;
			System.out.println(command);

			powerShellProcess = Runtime.getRuntime().exec(command);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void captureScreenshotPrintScreen(String destination) {
		Process powerShellProcess = null;
		try {

			String command = "(" + MainUtil.storeVariable.get("FILE_PATH_LOCATION")
					+ "\\PS_Files\\captureScreenshot1.ps1" + " '" + destination + "')";
			command = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -ExecutionPolicy ByPass "
					+ command;
			System.out.println(command);

			powerShellProcess = Runtime.getRuntime().exec(command);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	 * public static void captureScreenshot(String destination) { Process
	 * powerShellProcess = null; try { ClassLoader classLoader =
	 * ExtentScreenCapture.class.getClassLoader(); //
	 * System.out.println(classLoader.getResource("captureScreenshot.ps1").getPath()
	 * ); Keyboard k = new Keyboard(); k.doType(KeyEvent.VK_PRINTSCREEN);
	 * 
	 * // ps.captureScreenshot(destination); Thread.sleep(2000); String command =
	 * "Get-Clipboard -Format Image | ForEach-Object -MemberName Save -ArgumentList"
	 * + destination + "')"; command =
	 * "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -ExecutionPolicy ByPass "
	 * + command; System.out.println(command);
	 * 
	 * powerShellProcess = Runtime.getRuntime().exec(command);
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */
}
