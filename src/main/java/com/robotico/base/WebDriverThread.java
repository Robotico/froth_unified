package com.robotico.base;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.util.encoders.Hex;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.common.collect.ImmutableMap;
import com.robotico.lib.MainUtil;
import com.robotico.listener.ExtentTestNGITestListener;
import com.robotico.powershell.PowerShellBasicCommands;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.windows.WindowsDriver;
import io.appium.java_client.windows.WindowsElement;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

public class WebDriverThread {

	private BrowserMobProxy proxy;
	private static WebDriver existingdriver;;
	static String workspace = System.getProperty("user.dir");
	private static RemoteWebDriver driver;
	private static final String CHROME_DRIVER_STR = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_STR = "webdriver.gecko.driver";
	private static final String EDGE_DRIVER_STR = "webdriver.edge.driver";

	private static WindowsDriver windowDriverSeesion = null;
	private static final String IE_DRIVER_STR = "webdriver.ie.driver";

	private static Logger logger = LogManager.getLogger(WebDriverThread.class);
	private List<String> androidSessionIds = new LinkedList<>();

	static {
		/*
		 * LogManager.getLogger("org.apache.http.wire").setLevel(Level.OFF);
		 * java.util.logging.LogManager.getLogger("org.apache.http.headers").setLevel(
		 * Level.OFF);
		 * java.util.logging.LogManager.getLogger("org.apache.hc.client5.http.headers").
		 * setLevel(Level.OFF);
		 * java.util.logging.LogManager.getLogger("org.apache.hc.client5.http.wire").
		 * setLevel(Level.OFF);
		 * java.util.logging.LogManager.getLogger("com.zaxxer.hikari.pool.HikariPool").
		 * setLevel(Level.OFF);
		 */
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "ERROR");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "ERROR");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "ERROR");

	}

	public static WebDriver getExistingDriver() {
		try {
			System.setProperty(CHROME_DRIVER_STR, "E:\\chromedriver.exe");
			ChromeOptions chromeOptions = new ChromeOptions();

			chromeOptions.setExperimentalOption("debuggerAddress", "localhost:9222");

			existingdriver = new ChromeDriver(chromeOptions);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return existingdriver;
	}

	/**
	 * Driver instantiation module for WEB
	 *
	 * @param browserName Name of the driver ex: CHROME1,CHROME2,FIREFOX1
	 * @return Return the initiated driver
	 */
	RemoteWebDriver getDriver(String browserName) {
		logger.info(System.getProperty("os.name"));
		try {

			if (DriverFactory.getDriverPool().get(browserName) == null) {
				if (System.getProperty("location").equalsIgnoreCase("local")) {
					if ("firefox".equalsIgnoreCase(browserName)) {

						FirefoxOptions BrowserCapabilities = new FirefoxOptions();

						BrowserCapabilities.setCapability("marionette", true);

						WebDriverManager.firefoxdriver().clearResolutionCache().setup();
						driver = new FirefoxDriver(BrowserCapabilities);
					} else if ("chrome".equalsIgnoreCase(browserName)) {
						// System.setProperty(CHROME_DRIVER_STR, System.getProperty("chrome.path"));
						System.setProperty(CHROME_DRIVER_STR,System.getProperty("chrome.path"));
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
						chromePrefs.put("download.default_directory", MainUtil.downlaodfilepath);
						chromePrefs.put("download.prompt_for_download", Boolean.valueOf(false));

						ChromeOptions chromeOptions = new ChromeOptions();
						chromeOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						chromeOptions.setAcceptInsecureCerts(true);
						/*
						 * List<String> values = new ArrayList<String>();
						 * values.add("ignore-certificate-errors");
						 * values.add("disable-popup-blocking"); values.add("start-maximized");
						 */

						chromeOptions.addArguments("ignore-certificate-errors");
						chromeOptions.addArguments("disable-popup-blocking");
						chromeOptions.addArguments("start-maximized");

						chromeOptions.setExperimentalOption("prefs", chromePrefs);
						if (System.getProperty("headless") != null
								&& System.getProperty("headless").equalsIgnoreCase("yes"))
							chromeOptions.addArguments("headless");

						driver = new ChromeDriver(chromeOptions);
					} else if (browserName.equalsIgnoreCase("ie")) {
						InternetExplorerOptions options = new InternetExplorerOptions();
						options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
								true);

						options.setCapability("ignoreProtectedModeSettings", true);
						options.setCapability(CapabilityType.PLATFORM_NAME, "WINDOWS");
						options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						options.setCapability(CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						options.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
						WebDriverManager.iedriver().clearResolutionCache().setup();
						driver = new InternetExplorerDriver(options);
						DriverFactory.getDriverPool().put(browserName, driver);

					} else if (browserName.equalsIgnoreCase("edge")) {
						System.setProperty(EDGE_DRIVER_STR, System.getProperty("edge.path"));
						driver = new EdgeDriver();
						DriverFactory.getDriverPool().put(browserName, driver);
					}
				} else {
					if ("firefox".equalsIgnoreCase(browserName)) {
						FirefoxOptions firefoxOptions = new FirefoxOptions();
						FirefoxOptions opt = new FirefoxOptions();
						opt.setCapability(CapabilityType.BROWSER_NAME, "firefox");

						WebDriverManager.firefoxdriver().clearResolutionCache().setup();
						if (System.getProperty("env").equalsIgnoreCase("sit"))
							driver = new RemoteWebDriver(new URL("http://10.0.0.6:4651/wd/hub"), opt);
						else
							driver = new RemoteWebDriver(new URL("http://10.0.0.6:4652/wd/hub"), opt);

					} else if ("chrome".equalsIgnoreCase(browserName)) {

						if (System.getProperty("os.name").toLowerCase().contains("windows"))
							System.setProperty(CHROME_DRIVER_STR, System.getProperty("chrome.path"));
						else
							System.setProperty(CHROME_DRIVER_STR,
									Paths.get(workspace, "src", "main", "resources", "chromedriver").toString());

						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
						chromePrefs.put("download.default_directory", MainUtil.downlaodfilepath);
						chromePrefs.put("download.prompt_for_download", Boolean.valueOf(false));

						ChromeOptions chromeOptions = new ChromeOptions();
						System.out.println("reached here");
						if ("headless".equals(System.getProperty("state")))
							chromeOptions.addArguments("--headless");
						chromeOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						chromeOptions.setAcceptInsecureCerts(true);
						chromeOptions.addArguments("ignore-certificate-errors");
						chromeOptions.addArguments("disable-popup-blocking");
						chromeOptions.addArguments("start-maximized");
						chromeOptions.setExperimentalOption("prefs", chromePrefs);

						driver = new ChromeDriver(chromeOptions);

						driver.manage().deleteAllCookies();
					} else if (browserName.equalsIgnoreCase("edge")) {
						System.setProperty(EDGE_DRIVER_STR, System.getProperty("edge.path"));
						driver = new EdgeDriver();
						DriverFactory.getDriverPool().put(browserName, driver);
					}
				}
				System.out.println("going to maximise");
				driver.manage().window().maximize();
				DriverFactory.getDriverPool().put(browserName, driver);
				driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error creating the driver" + e);
		}
		return driver;
	}

	/**
	 * Driver instantiation module for mobile (Android)
	 *
	 * @param appName     Provide the mobile app name
	 * @param appPackage  Provide the mobile app package name
	 * @param appActivity Provide the app activity name
	 * @param nodeName    Provide the device serial number
	 * @return Return the initiated driver
	 */
	RemoteWebDriver getDriver(String appName, String appPackage, String appActivity, String deviceName) {

		DesiredCapabilities androidDcap = new DesiredCapabilities();
		String location = System.getProperty("location");
		if (location.equalsIgnoreCase("saucelab_a")) {

			androidDcap.setCapability("testobject_api_key", "D915F9DF5EC543F1AFEECCBE916E3280");
			androidDcap.setCapability("appiumVersion", "1.17.1");
			androidDcap.setCapability("noReset", "false");
			androidDcap.setCapability("testobject_session_creation_timeout", "900000");
			androidDcap.setCapability("testobject_app_id", System.getProperty("appID"));
			androidDcap.setCapability("testobject_suite_name", "Default Appium Suite");
			androidDcap.setCapability("testobject_test_name", "Default Appium Test");
			androidDcap.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");
			androidDcap.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
			androidDcap.setCapability("deviceName", deviceName);
		} else if (location.equalsIgnoreCase("browserstack")) {
			String username = System.getenv("BROWSERSTACK_USERNAME");
			String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
			String buildName = System.getenv("BROWSERSTACK_BUILD_NAME");
			String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
			String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");

			androidDcap.setCapability("device", "Google Pixel 3");
			androidDcap.setCapability("os_version", "9.0");
			androidDcap.setCapability("project", "My First Project");
			androidDcap.setCapability("build", "My First Build");
			androidDcap.setCapability("name", "Bstack-[Java] Sample Test");
			androidDcap.setCapability("app", "bs://bf80acf415df2d15982a38f9061ec4ff7055eb0a");
			androidDcap.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");

			androidDcap.setCapability("build", buildName); // CI/CD job name using
			androidDcap.setCapability("browserstack.local", browserstackLocal);
			androidDcap.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
			System.out.println("browserstack.local" + browserstackLocal);
			System.out.println("browserstack.localIdentifier" + browserstackLocalIdentifier);

		} else {
			androidDcap = new DesiredCapabilities();
			androidDcap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
			androidDcap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			androidDcap.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
			androidDcap.setCapability(MobileCapabilityType.NO_RESET, true);
			androidDcap.setCapability(MobileCapabilityType.FULL_RESET, false);
			androidDcap.setCapability(AndroidMobileCapabilityType.NO_SIGN, true);
			androidDcap.setCapability(AndroidMobileCapabilityType.APP_WAIT_DURATION, 10000);
			androidDcap.setCapability("deviceName", deviceName);
			androidDcap.setCapability("browserName", "");
			androidDcap.setCapability("newCommandTimeout", 1500);
			androidDcap.setCapability("appPackage", appPackage);
			androidDcap.setCapability("appActivity", appActivity);
			androidDcap.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
		}
		try {
			if (location.equalsIgnoreCase("local")) {
				System.out.println("device name is ssp");
				driver = new AndroidDriver<>(new URL("http://127.0.0.1:4651/wd/hub"), androidDcap);
			} else if (location.equalsIgnoreCase("saucelab_a")) {
				driver = new AndroidDriver<>(new URL("https://eu1.appium.testobject.com/wd/hub"), androidDcap);
			} else if (location.equalsIgnoreCase("browserstack")) {
				String userName = "subhrasubudhi1";
				String accessKey = "85Ufyfy8BsHbUtMh11Fw";
				/*
				 * driver = new AndroidDriver<>( new URL("https://" + userName + ":" + accessKey
				 * + "@hub-cloud.browserstack.com/wd/hub"), androidDcap);
				 */
				driver = new AndroidDriver<>(
						new URL("https://" + userName + ":" + accessKey + "@hub.browserstack.com/wd/hub"), androidDcap);

			} /*
				 * else if (System.getProperty("location").equalsIgnoreCase("remote")) { driver
				 * = new AndroidDriver<>(new
				 * URL(PropertyHelper.getProperties("REMOTE_HUB_URL")), androidDcap);
				 * 
				 * }
				 */

			logger.info("Session ID of Android: " + driver.getSessionId());
			androidSessionIds.add(driver.getSessionId().toString());
			logger.info("Session ID's of Android: " + androidSessionIds);
		} catch (WebDriverException e) {
			logger.error("Driver instantiating failed" + e);
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		DriverFactory.getDriverPool().put(appName, driver);
		return driver;
	}

	/**
	 * Driver instantiation module for mobile (iOS)
	 *
	 * @param appName    Provide the mobile app name
	 * @param uuid       Provide the uuid
	 * @param bundleId   Provide the bundle ID of the app
	 * @param xcodeOrgId Provide the xcode org ID
	 * @param timeout
	 * @return returns an object iOS driver
	 */
	RemoteWebDriver getDriver(String appName, String deviceName, int timeout) {
		DesiredCapabilities ioscap = new DesiredCapabilities();
		String location = System.getProperty("location");
		try {
			if (location.equalsIgnoreCase("local")) {

				/*
				 * ioscap.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, 8100);
				 * ioscap.setCapability(IOSMobileCapabilityType.BUNDLE_ID, bundleId);
				 * ioscap.setCapability(IOSMobileCapabilityType.XCODE_ORG_ID, xcodeOrgId);
				 * ioscap.setCapability(IOSMobileCapabilityType.XCODE_SIGNING_ID,
				 * "iPhone Developer"); ioscap.setCapability("automationName", "XCUITest");
				 * ioscap.setCapability("platformName", "iOS");
				 * ioscap.setAcceptInsecureCerts(true); ioscap.acceptInsecureCerts();
				 * ioscap.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 60000);
				 * ioscap.setCapability(IOSMobileCapabilityType.COMMAND_TIMEOUTS, 60000);
				 * ioscap.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, 1200000);
				 * ioscap.setCapability(IOSMobileCapabilityType.WDA_CONNECTION_TIMEOUT,
				 * 1200000); driver = new IOSDriver<>(new URL("http://127.0.0.1:4651/wd/hub"),
				 * ioscap);
				 */
			} else if (location.equalsIgnoreCase("iossim")) {

				ioscap.setCapability("appiumVersion", "1.13.0");
				ioscap.setCapability("deviceName", "iPhone XR Simulator");
				ioscap.setCapability("deviceOrientation", "portrait");
				ioscap.setCapability("platformVersion", "12.4");
				ioscap.setCapability("platformName", "iOS");
				ioscap.setCapability("browserName", "");
				ioscap.setCapability("app", "https://github.com/prakashpqa/demoproject/raw/master/ssp.zip");
				driver = new IOSDriver<>(new URL(
						"https://demousermano2:0b6a50de-62d8-4544-9551-7a428e584a59@ondemand.eu-central-1.saucelabs.com:443/wd/hub"),
						ioscap);
			} else if (location.equalsIgnoreCase("browserstack")) {
				ioscap.setCapability("device", "iPhone 11 Pro");
				ioscap.setCapability("os_version", "13");
				ioscap.setCapability("project", "My First Project");
				ioscap.setCapability("build", "My First Build");
				ioscap.setCapability("name", "Bstack-[Java] Sample Test");
				ioscap.setCapability("app", "bs://c1c367498d5e70a9656e0c723b49fd2945c1cc36");
			} else {
				ioscap.setCapability("testobject_api_key", "D915F9DF5EC543F1AFEECCBE916E3280");
				ioscap.setCapability("testobject_app_id", System.getProperty("appID"));
				ioscap.setCapability("appiumVersion", "1.17.1");
				ioscap.setCapability("automationName", "XCUITest");
				ioscap.setCapability("sendKeyStrategy", "grouped");
				ioscap.setCapability("interKeyDelay", "100");
				ioscap.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
				ioscap.setCapability("platformName", "iOS");
				ioscap.setCapability("name", "Support_Team_Test");
				ioscap.setCapability("app", "https://www.dropbox.com/s/9207dsgw4z9o8oy/ssp.ipa?dl=0");
				ioscap.setCapability("deviceName", deviceName);
				ioscap.setCapability("startIWDP", Boolean.TRUE);
				ioscap.setCapability("eventLoopIdleDelaySec", "4");
				ioscap.setCapability("wdaEventloopIdleDelay", "7");
				ioscap.setCapability("wdaEventloopIdleDelay", "7");
				ioscap.setCapability("waitForQuiescence", Boolean.FALSE);
				ioscap.setCapability("waitForQuietness", Boolean.FALSE);

				driver = new IOSDriver<>(new URL("https://eu1.appium.testobject.com/wd/hub"), ioscap);
			}
		} catch (Exception e) {
			logger.error("IOSDriver Driver instantiating failed" + e);
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		DriverFactory.getDriverPool().put(appName, driver);
		return driver;
	}

// windows driver initiation 
	public static WindowsDriver getWindowsDriver(String AppPath, String AppName, String... sleeptime) {
		try {

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("app", AppPath == null || AppPath.equalsIgnoreCase("") ? "Root" : AppPath);
			capabilities.setCapability("platformName", "Windows");
			capabilities.setCapability("deviceName", "WindowsPC");
			windowDriverSeesion = new WindowsDriver<WindowsElement>(new URL("http://127.0.0.1:4723"), capabilities);

			// Assert.assertNotNull(windowDriverSeesion);
			// Identify the current window handle. You can check through inspect.exe which
			// window this is.
			Thread.sleep(sleeptime.length > 0 ? Long.parseLong(sleeptime[0]) : 10000);

			try {
				Set<String> allWindowHandles = windowDriverSeesion.getWindowHandles();
				List<String> list = new ArrayList<String>(allWindowHandles);
				Thread.sleep(2000);
				windowDriverSeesion.switchTo().window(list.get(list.size() - 1));
				windowDriverSeesion.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

			} catch (Exception e) {
				// e.printStackTrace();
				System.out.println("List is empty" + e.getMessage());
			}
			Thread.sleep(2000);

			if (!(AppName.equalsIgnoreCase("root") || AppName.equalsIgnoreCase("Windows"))) {
				ExtentTestNGITestListener.createNode("Launching the driver for app: " + AppName);
				if (windowDriverSeesion == null)
					MainUtil.LogPass("App Launched Successfully" + AppName, AppName);
				else
					MainUtil.LogPass("App Launched Successfully" + AppName, AppName, windowDriverSeesion);
			}

			if (windowDriverSeesion == null || AppPath == null || AppPath.equalsIgnoreCase("")) {
				ExtentTestNGITestListener.createNode("Launching the driver for app :" + AppName);
				MainUtil.LogFail("Launch Unsuccessful ,check path", AppName);
			}

		} catch (Exception e) {
			e.printStackTrace();
			{
				ExtentTestNGITestListener.createNode("Launching the driver for app :" + AppName);
				MainUtil.LogFail(AppName + "App Launch UnSuccessful", AppName, e);
			}

		}
		return windowDriverSeesion;
	}

	public static WindowsDriver getExistingWindowsDriver(String AppName) {
		try {

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("deviceName", "WindowsPC");
			capabilities.setCapability("app", AppName);
			windowDriverSeesion = new WindowsDriver<WindowsElement>(new URL("http://127.0.0.1:4723"), capabilities);
			// Assert.assertNotNull(windowDriverSeesion);

			if (windowDriverSeesion != null) {
				Thread.sleep(7000);
				MainUtil.LogPass("Switched to", AppName, windowDriverSeesion);
			}

		} catch (

		Exception e) {
			e.printStackTrace();
			MainUtil.LogFail("Switching failed", AppName, e);

		}
		return windowDriverSeesion;
	}

	public static WindowsDriver getExsitingDriverByProcessId(String AppName) {
		try {
			int TopLevelWindowHandle;

			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			Process powerShellProcess = ps.executeCommandWithoutOutPut("get-process -Name '" + AppName + "'",
					"Get existing id", AppName, "N");
			String hexString = Hex.toHexString(powerShellProcess.toString().getBytes());

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("deviceName", "WindowsPC");
			capabilities.setCapability("appTopLevelWindow", hexString);
			windowDriverSeesion = new WindowsDriver<WindowsElement>(new URL("http://127.0.0.1:4723"), capabilities);

			Thread.sleep(7000);
			// MainUtil.LogPass("Switched to", AppName, windowDriverSeesion);

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return windowDriverSeesion;
	}

	BrowserMobProxy getProxy() {
		return this.proxy;
	}

	private Proxy startProxy() {
		logger.info("Proxy configuration");
		logger.info("Starting the proxy");
		proxy = new BrowserMobProxyServer();
		proxy.start(4444);
		logger.info("get the Selenium proxy object - org.openqa.selenium.Proxy");
		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
		return seleniumProxy;
	}

	/**
	 * Quit driver
	 */
	void quitDriver() {

		try {
			logger.info("quitDriver");
			if (!DriverFactory.getDriverPool().entrySet().isEmpty()) {
				for (Map.Entry<String, RemoteWebDriver> driverInstance : DriverFactory.getDriverPool().entrySet()) {
					if (driverInstance.getValue() != null || !driverInstance.getValue().equals("")) {
						if ((driverInstance.getValue() instanceof AndroidDriver)
								|| (driverInstance.getValue() instanceof IOSDriver))
							try {
								driverInstance.getValue().quit();
							} catch (WebDriverException wb) {
								logger.info("Session is already terminated");
							}
						else if (driverInstance.getValue() instanceof RemoteWebDriver) {
							try {
								driverInstance.getValue().quit();
							} catch (WebDriverException wb) {
								logger.info("Session is already terminated");
							}
						}
					}
				}
			}
			windowDriverSeesion.quit();
		} catch (Exception e) {
			logger.error("Exception in closing the driver \n" + e);
		}

	}

	public static void main(String[] args) {
		WebDriverThread w = new WebDriverThread();
		System.setProperty("browser", "chrome");
		System.setProperty("location", "local");

		driver = w.getDriver("chrome");
		driver.get("https://www.google.com/");

	}
}
