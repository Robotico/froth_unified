package com.robotico.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseObjectPage<T extends CustomLoadableComponent<T>> extends CustomLoadableComponent<T> {
	private static Logger logger = LoggerFactory.getLogger(BaseObjectPage.class);
	private WebDriver driver;

	public BaseObjectPage(RemoteWebDriver driver) {
		this.driver = driver;
	}

	public T openPage(Class<T> clazz, String pageName) {
		logger.info("Title = {}" + driver.getTitle() + clazz.getName());
		T page = null;
		try {
			page = PageFactory.initElements(driver, clazz);
			page = page.get(pageName);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return page;

	}

	public abstract String getPageUrl();

	public void open(String url) {
		driver.get(url);
	}

}