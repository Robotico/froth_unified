package com.robotico.base;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import net.lightbody.bmp.BrowserMobProxy;

public class DriverFactory {

	private static List<WebDriverThread> webDriverThreadPool = Collections
			.synchronizedList(new ArrayList<WebDriverThread>());

	private static ThreadLocal<WebDriverThread> driverThread;

	private static Logger logger = LogManager.getLogger(DriverFactory.class);

	protected static Map<String, RemoteWebDriver> driverPool;

	@BeforeSuite
	public static void instantiateDriverObject() {
		logger.info("Instantiating Driver Object");
		File dir = new File("extentreport");
		dir.mkdir();
		driverThread = ThreadLocal.withInitial(() -> {
			WebDriverThread webDriverThread = new WebDriverThread();
			webDriverThreadPool.add(webDriverThread);
			return webDriverThread;
		});
	}

	/**
	 * Returns the remote web driver
	 *
	 * @param browserName  Provide the browser for storing in the driver pool (Ex:
	 *                     chrome1, chrome2)
	 * @param designatedVM Pass the VM name on which this execution should happen
	 * @return returns the remotewebdriver
	 * @throws InvocationTargetException
	 */

	public static RemoteWebDriver getDriver(String browserName) {
		logger.info("Getting WebDriver");
		driverThread.get().getDriver(browserName);
		return getDriverPool().get(browserName);
	}

	/**
	 * Returns the mobile driver from the driver pool
	 *
	 * @param appName     Provide the app name
	 * @param appPackage  Provide the appPackage name
	 * @param appActivity Provide appActivity
	 * @return returns the driver from the driver pool
	 */
	public static RemoteWebDriver getDriver(String appName, String appPackage, String appActivity, String deviceName) {
		driverThread.get().getDriver(appName, appPackage, appActivity, deviceName);
		return getDriverPool().get(appName);
	}

	public static RemoteWebDriver getDriver(String appName, String deviceName, int timeout) {
		driverThread.get().getDriver(appName, deviceName, timeout);
		return getDriverPool().get(appName);
	}

	/*
	 * public static WindowsDriver getWindowsDriver(String appName) { return
	 * driverThread.get().getWindowsDriver(appName,String appName); }
	 */

	/**
	 * Get driver from driver pool stored in Map
	 *
	 * @return driver pool
	 */
	public static Map<String, RemoteWebDriver> getDriverPool() {
		if (driverPool == null) {
			driverPool = new HashMap<>();
		}
		return driverPool;
	}

	public static BrowserMobProxy getProxy() {
		return driverThread.get().getProxy();
	}

	@AfterSuite
	public static void closeDriverObjects() {
		logger.info("Close the Driver ***************");
		try {
			// WebDriverThread.videoReord.stopRecording();
			// System.out.println(new
			// GsonBuilder().create().toJsonTree(MainUtil.TestCaseNOs).getAsJsonArray().toString());
			logger.info("Updating the RemoteWebdriver ***************");
			for (WebDriverThread webDriverThread : webDriverThreadPool) {
				if (webDriverThread != null) {
					webDriverThread.quitDriver();
				} else {
					logger.info("Driver already closed");
				}
			}

			// SQLConnectionHelper.closeDBConnPool();
		} catch (Exception e) {
			logger.error("Error in terminating the driver objects \n" + e);
		}

	}

}