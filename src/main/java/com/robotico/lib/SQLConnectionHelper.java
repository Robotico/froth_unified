package com.robotico.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.robotico.listener.ExtentTestNGITestListener;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class SQLConnectionHelper extends TestDataSqlConnection {

	private static final Logger logger = LogManager.getLogger(SQLConnectionHelper.class);
	private static String roboticoForthDbSchema = "";
	private static String roboticoDbIp = "";
	private static String roboticoDbPort;
	private static String roboticoDbUsername = "";
	private static String roboticoDbPassword = "";
	private static HikariDataSource roboticoFrothDataSource;

	public static Connection getFrothShcemaDBConnection() {
		try {
			roboticoForthDbSchema = "froth";
			if (roboticoFrothDataSource == null || roboticoFrothDataSource.isClosed()) {
				try {
					roboticoFrothDataSource = setMySqlConnDS(roboticoDbIp, roboticoDbPort, roboticoForthDbSchema,
							roboticoDbUsername, roboticoDbPassword, 8, 40);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("There is issue in getting tmanoDataSource db pool connection");
				}
			}
			logger.info("Getting the PMAS New database connection from the pool");
			return roboticoFrothDataSource.getConnection();
		} catch (SQLException sqe) {
			logger.error("Error in getting the database connection from the pool\n"+ sqe);
		}
		return null;
	}

	private static HikariDataSource setMySqlConnDS(String ip, String port, String schema, String userName,
			String password, int min, int max) {
		HikariDataSource hikariDataSource = null;

		try {
			logger.info("calling setMySqlConnDS");
			HikariConfig config = new HikariConfig();
			config.setDriverClassName("com.mysql.cj.jdbc.Driver");
			config.setJdbcUrl("jdbc:mysql://" + ip + ":" + port + "/" + schema + "?autoReconnect=true&useSSL=false");
			config.setMinimumIdle(0);
			config.setMaximumPoolSize(100);
			config.setUsername(userName);
			config.setPassword(password);
			config.addDataSourceProperty("cachePrepStmts", "true");
			config.addDataSourceProperty("prepStmtCacheSize", "250");
			config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
			hikariDataSource = new HikariDataSource(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hikariDataSource;

	}

	/**
	 * To get the module info based on the scenario
	 * 
	 * @param sceanrioName
	 * @return
	 */
	public static String getModuleName(String sceanrioName) {
		String modulename = "";
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "SELECT m.`name`as moduleName FROM `modules` m , `test_scenarios` s WHERE m.id=s.module_id AND s.`testscenario_name`=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, sceanrioName);
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						modulename = rs.getString("moduleName");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return modulename;

	}

	/**
	 * Store the performance monitpring results back into db
	 * 
	 * @param pagename
	 * @param totalPageLoadTime_decimal
	 * @param expected_time
	 * @param totalPageSize_decimal
	 * @param status
	 * @param fileLocation
	 */
	public static void storePMASintoDB(String pagename, double totalPageLoadTime_decimal, String expected_time,
			double totalPageSize_decimal, String status, File fileLocation) {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "insert into pmas_details (id,execution_id,page_name,actual_time,expected_time,page_size,status,siteid,harfile,harfileLocation) values(uuid(),?,?,?,?,?,?,?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, pagename);
				stmt.setString(3, totalPageLoadTime_decimal + "");
				stmt.setString(4, expected_time);
				stmt.setString(5, totalPageSize_decimal + "");
				stmt.setString(6, status);
				stmt.setString(7, MainUtil.SITE_ID);
				InputStream inputStream = new FileInputStream(fileLocation);
				stmt.setBlob(8, inputStream);
				stmt.setString(9, fileLocation.getPath());

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("insertion unsuccessful to the pmas table");

				// fileLocation.deleteOnExit();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Store testresults for each step in db
	 * 
	 * @param steps
	 * @param stepResult
	 */
	public static void storeTestResultsInDB(String steps, String stepResult) {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "insert into testresult_details (id,execution_id,scenario,testcase,steps,status,vm_no,siteid) values(uuid(),?,?,?,?,?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, MainUtil.scenarioDetails.get("Scenario"));
				stmt.setString(3, MainUtil.testcaseName);
				stmt.setString(4, steps);
				stmt.setString(5, stepResult);
				stmt.setString(6, MainUtil.vm_no.replace("VM", ""));
				stmt.setString(7, MainUtil.SITE_ID);

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("insertion unsuccessful to the testresults_details table");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void InsertToExecutionDetails() {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "insert into  executionDetails(executionId,siteid,vm_no,status) values(?,?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, MainUtil.SITE_ID);
				stmt.setString(3, MainUtil.vm_no.replace("VM", ""));
				stmt.setString(4, "InProgress");

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Inserting into executionDetails table completed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void UpdateToInInExecutionDetails(String status) {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "update executionDetails set status=? where executionId=? and siteid=? and vm_no=? and status=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, status);
				stmt.setString(2, ExtentTestNGITestListener.executionId);
				stmt.setString(3, MainUtil.SITE_ID);
				stmt.setString(4, MainUtil.vm_no.replace("VM", ""));
				stmt.setString(5, status.equalsIgnoreCase("InProgress") ? "New" : "InProgress");

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Updation unsuccessful to the executionDetails table");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
}
