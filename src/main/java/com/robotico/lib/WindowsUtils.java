package com.robotico.lib;

import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.robotico.base.DriverFactory;
import com.robotico.lib.MainUtil.ProjectConst;
import com.robotico.listener.ExtentScreenCapture;
import com.robotico.listener.ExtentTestNGITestListener;

import io.appium.java_client.windows.WindowsDriver;

public class WindowsUtils extends basicUtils {
	private static Logger logger = LogManager.getLogger(WindowsUtils.class);

	public static void LogPass(String comment, String elementName, WindowsDriver... driver) {
		try {
			// ss = SS.equalsIgnoreCase("Y") ? "true" : "false";
			if (ss.equalsIgnoreCase("true")) {
				System.out.println(driver.length);
				if (driver.length > 0) {
					Thread.sleep(4000);

					ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment,
							ExtentScreenCapture.captureSrceenPass(elementName, driver[0]));
				} else {
					System.out.println("Driver is null .. taking screenshot with powershell");
					Thread.sleep(4000);
					ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment,
							ExtentScreenCapture.captureSrceenPass(elementName));
				}
			} else if (ss.equalsIgnoreCase("fail") || ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);
			else
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);

			store2db(comment, "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void LogPassWithPrintScreen(String comment, String elementName) {
		try {
			// ss = SS.equalsIgnoreCase("Y") ? "true" : "false";
			if (ss.equalsIgnoreCase("true")) {

				System.out.println("Driver is null .. taking screenshot with printscreen");
				Thread.sleep(4000);
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment,
						ExtentScreenCapture.captureSrceenPassWithPrintCreen(elementName));
			} else if (ss.equalsIgnoreCase("fail") || ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);
			else
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);

			store2db(comment, "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void LogFailWithPrintScreen(String comment, String elementName) {
		try {
			// ss = SS.equalsIgnoreCase("Y") ? "true" : "false";
			if (ss.equalsIgnoreCase("true")) {

				System.out.println("Driver is null .. taking screenshot with printscreen");
				Thread.sleep(4000);
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment,
						ExtentScreenCapture.captureSrceenFailWithPrintCreen(elementName));
			} else if (ss.equalsIgnoreCase("fail") || ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment);
			else
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment);

			store2db(comment, "fail");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void LogFail(String comment, String elementName, Exception... e) {
		logger.error(comment);
		elementName = elementName == null || elementName.equalsIgnoreCase("") ? "" : elementName + "-";
		logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + (e.length > 0 ? e[0] : ""));
		if (e.length > 0)
			e[0].printStackTrace();

		if (ss.equalsIgnoreCase("true") || ss.equalsIgnoreCase("fail")) {
			System.out.println("Inside true to take screenshot");
			ExtentTestNGITestListener.getTest().get().fail(elementName + comment,
					ExtentScreenCapture.captureSrceenFail(elementName));

		} else if (ss.equalsIgnoreCase("false"))
			ExtentTestNGITestListener.getTest().get().fail(elementName + comment);
		else
			ExtentTestNGITestListener.getTest().get().fail(elementName + comment);

		store2db(comment, "fail");

	}

	public static void LogFail(String comment, String elementName, WindowsDriver driver, Exception... e) {
		logger.error(comment);
		logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + (e.length > 0 ? e[0] : ""));
		elementName = elementName == null || elementName.equalsIgnoreCase("") ? "" : elementName + "-";
		if (e.length > 0)
			e[0].printStackTrace();

		if (ss.equalsIgnoreCase("true") || ss.equalsIgnoreCase("fail")) {

			if (driver == null) {
				System.out.println("Driver is null .. taking screenshot with powershell");
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment,
						ExtentScreenCapture.captureSrceenFail(elementName));
			} else
				ExtentTestNGITestListener.getTest().get().fail(elementName + comment,
						ExtentScreenCapture.captureSrceenFail(elementName, driver));

		} else if (ss.equalsIgnoreCase("false"))
			ExtentTestNGITestListener.getTest().get().fail(elementName + comment);
		else
			ExtentTestNGITestListener.getTest().get().fail(elementName + comment);

		store2db(comment, "fail");

	}

	/**
	 * Method to input text by xpath with Store Variable
	 *
	 * @param element     Pass the element name against which you want to pass the
	 *                    text
	 * @param text        Pass the text to be entered
	 * @param elementName Pass the element name for logging
	 * @param storeVar    Pass the store variable
	 * @param driver      Pass the driver
	 */
	public static synchronized void inputTextByXpath(WebElement element, String text, String elementName,
			String storeVar, WindowsDriver driver) {
		try {
			logger.info("Input by xpath");
			element.sendKeys(text);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, element.getAttribute("value"));
				logger.info(new StringBuilder("storeVar======>").append(storeVariable.get(storeVar)).toString());
			}

			LogPass(text + " Typed Successfully", elementName, driver);

		} catch (Exception e) {
			LogFail(text + " Typed unsuccessful", elementName, driver, e);

		}
	}

	public static synchronized boolean clickByXpath(WebElement elementToClick, String elementName,
			WindowsDriver driver) {
		boolean status = false;
		try {
			logger.info("Click by xpath on: " + elementName);
			if (System.getProperty("pmas").equalsIgnoreCase("true"))
				DriverFactory.getProxy().newHar(elementName);
			elementToClick.click();
			status = true;
			LogPass(elementName + " Clicked Successfully", elementName, driver);

		} catch (Exception e) {
			LogFail(elementName + " Click Unsuccessful", elementName, driver, e);
			status = false;
			//throw e;
		}
		return status;
	}

	public static synchronized boolean checkForText(String storeVar, WebElement element, String data,
			String elementName, WindowsDriver driver) {
		boolean status = true;
		try {
			logger.info("Check for text " + data);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").equalsIgnoreCase(data.trim().replaceAll("\\s", ""))) {
					System.out.println("Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().matches(data.trim().toLowerCase())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);
				} else if (element.getText().trim().equalsIgnoreCase(data.trim())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
			} else {

				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

			status = false;
		}
		return status;
	}

	public static synchronized void checkForElement(String storeVar, WebElement element, String elementName,
			WindowsDriver driver) {
		try {
			logger.info("Check for the element: " + elementName);
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				logger.info("line 3511 --inside the if where storevariable is not null");
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			if (element == null) {
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);
			} else if (element.isDisplayed()) {
				logger.info("### The Element is verified successfully");
				LogPass(elementName + "Element is verified successfully", elementName, driver);
			} else {
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the element ", elementName, driver, e);
		}
	}

	public static synchronized boolean checkForContainsText(String storeVar, WebElement element, String data,
			String elementName, WindowsDriver driver) {
		boolean status = true;
		try {
			logger.info("Contains Check for text " + data);
			String actualOutput = element.getText();
			System.out.println("Actual value is---->" + actualOutput);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Contains Text: Target element verification first:" + element);
			if (element.isDisplayed()) {

				if (element.getText().toLowerCase().trim().contains(data.toLowerCase().trim())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);
				} else if (actualOutput.trim().replaceAll("\\s", "").toLowerCase()
						.contains(data.toLowerCase().trim().replaceAll("\\s", ""))) {
					logger.info("Contains Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);
				} else if (element.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
					logger.info("####Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);
				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName + " Expected data " + data
					+ " and Element is " + element, elementName, driver);
			status = false;
		}
		return status;
	}

	/**
	 * Method to click on element twice
	 *
	 * @param element pass the elemet which need to double clicked
	 * @param driver  pass the driver
	 */
	public static synchronized void doubleClickOnElement(WebElement element, String elementName, WindowsDriver driver) {
		try {
			Actions actions = new Actions(driver);
			actions.doubleClick(element).perform();

			LogPass(elementName + " Double click successful", elementName, driver);
		} catch (Exception e) {
			LogFail("doubleClickOnElement : Double click failed", "", driver, e);
			throw e;
		}
	}

	public static synchronized void verifyElementIsNotDisplayed(WebElement element, String ElementName,
			WindowsDriver driver) {
		boolean ElementDisplayed = false;
		try {
			if (element.isDisplayed()) {
				ElementDisplayed = true;
			}

		} catch (Exception ex) {
			ElementDisplayed = false;
			System.out.println(ElementName + "Element not present...");
		}
		if (ElementDisplayed == false) {
			LogPass(ElementName + " Element is not present", ElementName, driver);
		} else {
			LogFail(ElementName + " Element is present", ElementName, driver);

		}
	}

	public static synchronized void mouseHoveronElementPrintScrren(WebElement mousehover, String element,
			WindowsDriver driver) {
		try {
			logger.info(new StringBuilder("Mouse hovering no: ").append(mousehover).toString());
			Actions action = new Actions(driver);
			logger.info("Moving the mouse");
			action.moveToElement(mousehover).build().perform();

			Thread.sleep(5000);
			LogPassWithPrintScreen("mouse hover Successfully", element.toLowerCase());
		} catch (Exception e) {
			e.printStackTrace();
			LogFailWithPrintScreen("mouse hover Unsuccessful", element.toLowerCase());
		}
	}

	public static synchronized void rightClickOnElement(WebElement element, String elementName, WindowsDriver driver) {
		try {
			Thread.sleep(2000);
			Actions actions = new Actions(driver);
			actions.contextClick(element).perform();
			Thread.sleep(3000);

			LogPassWithPrintScreen("Right click successful", elementName.toLowerCase());

		} catch (Exception e) {
			e.printStackTrace();
			LogFailWithPrintScreen("Right click Unsuccessful", elementName.toLowerCase());
		}
	}

	public static synchronized void checkElementNotPresent(String elementXpath, String elementName,
			WindowsDriver driver) {
		try {
			logger.info("Check for the element:" + elementName);
			if (!(driver.findElements(By.xpath(elementXpath)).size() > 0)) {

				LogPass(elementName + "Element is not present - successful", elementName, driver);

			} else {
				WebElement element = driver.findElement(By.xpath(elementXpath));
				logger.info("### The Element is present -  unsuccessful");

				LogFail(elementName + "The Element is present -  unsuccessful", elementName, driver);

			}
		} catch (NoSuchElementException e) {

			LogFail("Error occured while checking the element", elementName, driver, e);
			// throw e;
		}
	}

	/**
	 * 
	 * @param element     element xapath
	 * @param elementName element name
	 * @param xoffset     ex:30
	 * @param yoffset     - ex:0
	 * @param driver
	 */
	public static synchronized void dragAndDrop(WebElement element, String elementName, int xoffset, int yoffset,
			WindowsDriver driver) {
		try {
			Thread.sleep(4000);
			Actions action = new Actions(driver);
			Action dragAndDrop = (Action) action.dragAndDropBy(element, xoffset, yoffset).build();
			dragAndDrop.perform();
			Thread.sleep(4000);

			LogPassWithPrintScreen(elementName + " drag and drop successful", elementName.toLowerCase());

		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();

			LogFailWithPrintScreen("Unable to drag and drop", elementName.toLowerCase());
		}
	}

	public static synchronized void hitEnter(WindowsDriver driver) {

		try {
			Thread.sleep(2000);
			Actions action = new Actions(driver);
			action.sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(5000);
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "hitEnter" + e);
			e.printStackTrace();
		}

	}

}
