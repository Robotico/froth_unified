package com.robotico.lib;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aspose.cells.ImageOrPrintOptions;
import com.aspose.cells.ImageType;
import com.aspose.cells.SheetRender;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.robotico.base.DriverFactory;
import com.robotico.listener.ExtentTestNGITestListener;

public class MainUtil extends MobileUtils {

	private static Logger logger = LogManager.getLogger(MainUtil.class);

	public enum ProjectConst {
		EXCEPTIONTEXT("**** Exception Occurred ****"),
		ELEMENTNOTFOUNDEXCEPTIONTXT("**** Element Not Found or Xpath String passed in null ****"),
		EXCEPTIONTEXTMETHOD("**** Exception Occurred in the Method ****"), VALUE(" ======> ");

		private String msg;

		ProjectConst(String s) {
			msg = s;
		}

		public String getMsg() {
			return msg;
		}
	}

	/**
	 * Method to launch the URL
	 *
	 * @param url    Provide the URL
	 * @param driver Pass the driver
	 */
	public static synchronized void launchURL(String url, RemoteWebDriver driver) {
		try {
			logger.info("Launching the URL");
			if (System.getProperty("pmas").equalsIgnoreCase("true"))
				DriverFactory.getProxy().newHar(url);
			driver.get(url);

			LogPass("URL Launched Successfully", "URL", driver);
		} catch (Exception e) {
			LogFail("URL Launched Unsuccessfully", "URL", driver, e);
		}
	}

	/**
	 * Method for sending enter key
	 *
	 * @param element Pass the element against which you want to press enter
	 */
	public static synchronized void sendEnterKey(WebElement element) {
		try {
			logger.info("Entering the keys");
			element.sendKeys(Keys.ENTER);
			store2db("send enter key", "pass");
		} catch (NoSuchElementException e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "sendEnterKey" + e);
			store2db("send enter key", "fail");
		}
	}

	public static synchronized void hitEnter(RemoteWebDriver driver) {

		try {
			Thread.sleep(2000);
			Actions action = new Actions(driver);
			action.sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(5000);
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "hitEnter" + e);
			e.printStackTrace();
		}

	}

	public static synchronized void closeOpenedApplication(String appName) {
		try {

			// ExtentTestNGITestListener.createNode("Closing "+ appName);
			Thread.sleep(2000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			robot.keyPress(KeyEvent.VK_F4);
			robot.keyRelease(KeyEvent.VK_F4);
			robot.keyRelease(KeyEvent.VK_ALT);

			MainUtil.LogPass("Closed Application", appName);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static synchronized void hitEsc() {
		try {

			// ExtentTestNGITestListener.createNode("Closing "+ appName);
			Thread.sleep(2000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ESCAPE);
			robot.keyRelease(KeyEvent.VK_ESCAPE);

			MainUtil.LogPass(" Hit Successful", "Esc");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static synchronized void hitEnter() {
		try {

			// ExtentTestNGITestListener.createNode("Closing "+ appName);
			Thread.sleep(2000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static synchronized void hitBackSpace() {
		try {

			// ExtentTestNGITestListener.createNode("Closing "+ appName);
			Thread.sleep(2000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_BACK_SPACE);
			robot.keyRelease(KeyEvent.VK_BACK_SPACE);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static synchronized void hitTab(RemoteWebDriver driver) {

		try {
			Thread.sleep(2000);
			Actions action = new Actions(driver);
			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(5000);
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "hitTab" + e);
			e.printStackTrace();
		}

	}

	public static synchronized void hitTab() {

		try {
			Thread.sleep(2000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(5000);
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "hitTab" + e);
			e.printStackTrace();
		}

	}

	public static synchronized void sendTabKey(WebElement element) {
		try {
			logger.info("Entering the tab keys");
			element.sendKeys(Keys.TAB);
			store2db("send TAB key", "pass");
		} catch (NoSuchElementException e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD + " " + "sendTabKey" + e);
			store2db("send TAB key", "fail");
		}
	}

	/**
	 * To check element is displayed or not
	 *
	 * @param element pass the element
	 * @return return the boolean
	 */
	public static boolean IsElementDisplayed(WebElement element, String... elementName) {
		boolean ElementDisplayed = false;
		try {
			if (element.isDisplayed()) {
				ElementDisplayed = true;
			}
		} catch (Exception ex) {
			ElementDisplayed = false;
			logger.error("Got an Exception!!");
			System.out.println((elementName.length > 0 ? elementName[0] : "Element") + " is not visible...");
		}
		return ElementDisplayed;
	}

	/**
	 * Method to input text by xpath with Store Variable
	 *
	 * @param element     Pass the element name against which you want to pass the
	 *                    text
	 * @param text        Pass the text to be entered
	 * @param elementName Pass the element name for logging
	 * @param storeVar    Pass the store variable
	 * @param driver      Pass the driver
	 */
	public static synchronized void inputTextByXpath(WebElement element, String text, String elementName,
			String storeVar, RemoteWebDriver driver) {
		try {
			logger.info("Input by xpath");
			element.sendKeys(text);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, element.getAttribute("value"));
				logger.info(new StringBuilder("storeVar======>").append(storeVariable.get(storeVar)).toString());
			}

			LogPass(text + " Typed Successfully", elementName, driver);

		} catch (Exception e) {
			LogFail(text + " Typed unsuccessful", elementName, driver, e);

		}
	}

	public static synchronized void inputTextByxpathByCtrlDelete(WebElement element, String text, String elementName,
			String storeVar, RemoteWebDriver driver) {
		try {
			element.sendKeys(Keys.CONTROL, "a");
			element.sendKeys(Keys.DELETE);
			element.sendKeys(text);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, element.getAttribute("value"));
				logger.info(new StringBuilder("storeVar======>").append(storeVariable.get(storeVar)).toString());
			}
			LogPass("Typed Successfully", elementName, driver);

		} catch (Exception e) {
			LogFail("Typed unsuccessful", elementName, driver, e);
		}

	}

	public static synchronized void inputPassword(WebElement element, String password, String elementName,
			String storeVar, RemoteWebDriver driver) {
		try {
			logger.info("Entering the password");
			element.sendKeys(password);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, element.getAttribute("value"));
				logger.info(new StringBuilder("storeVar ======> ").append(storeVariable.get(storeVar)).toString());
			}
			LogPass("Password Typed Successfully", elementName, driver);
		} catch (Exception e) {
			LogFail("Password Type UnSuccessful", elementName, driver, e);

		}
	}

	/**
	 * Method to click on an element
	 *
	 * @param elementToClick Pass the element to be clicked
	 * @param elementName    Pass the element name for logging purpose
	 * @param driver         Pass the driver
	 * @return returns the boolean value
	 */
	public static synchronized boolean clickByXpath(WebElement elementToClick, String elementName,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info("Click by xpath on: " + elementName);
			if (System.getProperty("pmas").equalsIgnoreCase("true"))
				DriverFactory.getProxy().newHar(elementName);
			elementToClick.click();
			status = true;
			LogPass(elementName + " Clicked Successfully", elementName, driver);

		} catch (Exception e) {
			LogFail(elementName + " Click Unsuccessful", elementName, driver, e);

			throw e;
		}
		return status;
	}

	/**
	 * Javascript code for clicking
	 *
	 * @param elementToClick Pass the webelement to be clicked
	 * @param elementName    Pass the element name
	 * @param driver         Pass the driver object reference
	 * @return
	 */
	public static synchronized boolean javaScriptClick(WebElement elementToClick, String elementName,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info("Click by xpath using JavaScript: " + elementName);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", elementToClick);
			status = true;
			LogPass(elementName + "JavaScript Click Successful", elementName, driver);

		} catch (UnhandledAlertException e) {
			clickOnAlert("Ok", "Ok on alert", driver);
			LogPass(elementName + "JavaScript Click Successful", elementName, driver);

		} catch (Exception e) {

			LogFail(elementName + "JavaScript Click Unsuccessful", elementName, driver, e);

		}
		return status;
	}

	/**
	 * Method to click on the element and open in the new tab
	 *
	 * @param elementToClick :Pass the element to be clicked
	 * @param elementName    :Pass the element name for logging purpose
	 * @param driver         :Pass the driver
	 * @return :returns the boolean value
	 */
	public static synchronized boolean clickAndOpenInNewTab(WebElement elementToClick, String elementName,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info("click And Open In NewTab");
			Actions action = new Actions(driver);
			action.keyDown(Keys.CONTROL).build().perform();
			elementToClick.click();
			action.keyUp(Keys.CONTROL).build().perform();
			status = true;
			LogPass(elementName + "Click Successful", elementName, driver);

		} catch (UnhandledAlertException e) {
			clickOnAlert("Ok", "Ok on alert", driver);
			LogPass(elementName + "Click Successful", elementName, driver);

		} catch (Exception e) {
			LogFail(elementName + "Click Unsuccessful", elementName, driver, e);

		}
		return status;
	}

	/**
	 * Method to scroll and click
	 *
	 * @param elementToClick Pass the element to which it has to be scrolled
	 * @param elementName    Pass the element name for logging purpose
	 * @param driver         Pass the driver
	 * @return returns the boolean value
	 */
	public static synchronized boolean scrollandClick(WebElement elementToClick, String elementName,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info(new StringBuilder("Scroll and click: ").append(elementToClick).toString());
			WebDriverWait wait = new WebDriverWait(driver, 50);
			Point hoverItem = elementToClick.getLocation();
			((JavascriptExecutor) driver).executeScript("return window.title;");
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY() - 400) + ");");
			wait.until(ExpectedConditions.elementToBeClickable(elementToClick));

			if (System.getProperty("pmas").equalsIgnoreCase("true"))
				DriverFactory.getProxy().newHar(elementName);
			elementToClick.click();
			LogPass(elementName + "Click Successful", elementName, driver);
			status = true;
		} catch (Exception e) {
			LogFail(elementName + "Click Unsuccessful", elementName, driver, e);
			throw e;
		}
		return status;
	}

	/**
	 * Method to return text value from an element
	 *
	 * @param storeTextFromElement Pass the element from which the text need to be
	 *                             stored
	 * @return returns the text from the after fetching from the element
	 */
	public static synchronized String storeTextValue(WebElement storeTextFromElement) {
		String text = "NA";
		try {
			logger.info(new StringBuilder("Store text value in: ").append(storeTextFromElement).toString());
			text = storeTextFromElement.getText();
		} catch (Exception e) {
			logger.error("Element not stored");
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "storeTextValue" + e);
			throw e;
		}
		return text;
	}

	/**
	 * Method to mousehover and click on an element by Xpath
	 *
	 * @param mousehover     pass the element on which the mouse needs to hover
	 * @param elementTOclick Pass the element to be clicked
	 * @param driver         pass the driver
	 * @throws InterruptedException throws InterruptedException
	 */
	public static synchronized void mouseHoverandClickbyxpath(WebElement mousehover, WebElement elementTOclick,
			RemoteWebDriver driver) {
		try {
			logger.info(new StringBuilder("Mouse hover and click by xpath on: ").append(elementTOclick).toString());
			Thread.sleep(3000);
			logger.info("Instantiating the Actions driver");
			Actions action = new Actions(driver);
			logger.info("Moving the mouse");
			action.moveToElement(mousehover).build().perform();
			Thread.sleep(3000);
			logger.info("clicking on the element");
			elementTOclick.click();
			LogPass("Mouse Hover and Clicked Successfully", "", driver);
			Thread.sleep(1000);
		} catch (Exception e) {
			LogFail("Mouse Hover and Click Unsuccessful", "", driver, e);
		}
	}

	/**
	 * Method to mousehover on an element
	 *
	 * @param mousehover Pass the element on which the mouse needs to hover
	 * @param driver     Pass the driver
	 */
	public static synchronized void mouseHoveronElement(WebElement mousehover, RemoteWebDriver driver) {
		try {
			logger.info(new StringBuilder("Mouse hovering no: ").append(mousehover).toString());
			Actions action = new Actions(driver);
			logger.info("Moving the mouse");
			action.moveToElement(mousehover).build().perform();
			LogPass("Data found Successfully", "", driver);
		} catch (Exception e) {
			LogFail("Data found Unsuccessful", "", driver, e);
		}
	}

	/**
	 * Method to switch to certain frame by Xpath
	 *
	 * @param switchFrame Pass the target i-frame name where need to switch
	 * @param driver      Pass the driver
	 */
	public static synchronized void switchToFramebyXpath(WebElement switchFrame, RemoteWebDriver driver) {
		try {
			logger.info(new StringBuilder("Switch to frame by xpath: ").append(switchFrame).toString());
			driver.switchTo().frame(switchFrame);

			LogPass("Switch to Frame Successful", "", driver);
		} catch (Exception e) {
			LogFail("Switch to Frame UnSuccessful", "", driver, e);
		}
	}

	/**
	 * Method to switch back to DefaultContent / Parent frame
	 *
	 * @param driver pass the driver
	 */
	public static synchronized void switchBackFromFrame(RemoteWebDriver driver) {
		try {
			logger.info("Switch back from frame");
			driver.switchTo().defaultContent();
			LogPass("Switch back from  Frame Successful", "", driver);
		} catch (Exception e) {
			LogFail("Switchback from Frame UnSuccessful", "", driver, e);

		}
	}

	/**
	 * Method to select drop down option by its value or visible text using
	 *
	 * @param dropDown      Pass the element of dropdown
	 * @param dropdownValue Pass the element of value of the dropdown
	 * @param driver        Pass the driver
	 * @return
	 */
	public static synchronized boolean selectValueByDropdown(WebElement dropDown, String dropdownValue,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info(new StringBuilder("Select value by dropdown: ").append(dropDown).toString());
			WebElement dropdownValues = dropDown;
			Select dropdowm = new Select(dropdownValues);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(dropDown));
			dropdowm.selectByVisibleText(dropdownValue);
			LogPass("Selected value " + dropdownValue + " from Dropdown Successfully", "", driver);
			status = true;
		} catch (Exception e) {
			status = false;
			LogFail("Selected value " + dropdownValue + " from Dropdown UnSuccessfully", "", driver, e);

		}
		return status;
	}

	/**
	 * To select from dropdown based on index
	 * 
	 * @param dropDown xpath of select tag
	 * @param index    index of the list to select
	 * @param driver   driver session
	 * @return returns true is seletced else false
	 */
	public static synchronized boolean selectByIndex4mDropdown(WebElement dropDown, int index, String elementName,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info(new StringBuilder("select By Index 4m Dropdown: ").append(dropDown).toString());
			WebElement dropdownValues = dropDown;
			Select dropdowm = new Select(dropdownValues);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(dropDown));
			dropdowm.selectByIndex(index);
			String select_option = dropdowm.getOptions().get(index).getAttribute("innerText");
			LogPass("Selected value " + select_option + " from Dropdown Successfully", elementName, driver);

			status = true;
		} catch (Exception e) {
			status = false;
			LogFail("Selected value from Dropdown UnSuccessfully", elementName, driver, e);

		}
		return status;
	}

	public static synchronized void checkForContainsTextBasedOnAttribute(String storeVar, WebElement element,
			String data, String attributeName, String elementName, RemoteWebDriver driver) {
		try {
			logger.info("Check for text " + data);
			String actualOutput = element.getAttribute(attributeName);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Contains Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				if (actualOutput.replace("\n", "").replace("\r", "").trim().contains(data.trim())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else {
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");

				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

			}
		} catch (Exception e) {
			LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver, e);
		}
	}

	/**
	 * Method to verify for Contains text in web
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data to compare
	 * @param elementName Pass the element for which you want to check the text
	 * @param driver      Pass the driver
	 * @return
	 */
	public static synchronized boolean checkForContainsText(String storeVar, WebElement element, String data,
			String elementName, RemoteWebDriver driver) {
		boolean status = true;
		try {
			logger.info("Contains Check for text " + data);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Contains Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");

				if (element.getText().trim().contains(data.trim())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (actualOutput.trim().replaceAll("\\s", "").contains(data.trim().replaceAll("\\s", ""))) {
					logger.info("Contains Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
					logger.info("####Contains Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver);
			status = false;
		}
		return status;
	}

	/**
	 * Method to verify for StartsWith text in web
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data to compare
	 * @param elementName Pass the element for which you want to check the text
	 * @param driver      Pass the driver
	 * @return
	 */
	public static synchronized boolean checkForStartsWithText(String storeVar, WebElement element, String data,
			String elementName, RemoteWebDriver driver) {
		boolean status = true;
		try {
			logger.info("Contains Check for text " + data);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for startsWith Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				if (actualOutput.trim().replaceAll("\\s", "").startsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("startsWith Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().startsWith(data.trim().toLowerCase())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().startsWith(data.trim())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");

				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

			status = false;
		}
		return status;
	}

	/**
	 * Method to verify for EndsWith text in web
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data to compare
	 * @param elementName Pass the element for which you want to check the text
	 * @param driver      Pass the driver
	 * @return
	 */
	public static synchronized boolean checkForEndsWithText(String storeVar, WebElement element, String data,
			String elementName, RemoteWebDriver driver) {
		boolean status = true;
		try {
			logger.info("Contains Check for text " + data);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for EndsWith Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				if (actualOutput.trim().replaceAll("\\s", "").endsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("startsWith Text matches with the given text");

					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().endsWith(data.trim().toLowerCase())) {
					logger.info("#### EndsWith Text Found in the Target location ####");

					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().endsWith(data.trim())) {
					logger.info("#### EndsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### EndsWith Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");

				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);
			status = false;
		}
		return status;
	}

	/**
	 * Method to select drop down value by option using tagName
	 *
	 * @param element Pass the element of dropdown
	 * @param text    Pass the text which need to select from the dropdown
	 * @param driver  Pass the driver
	 * @return
	 */
	public static synchronized boolean fetchOptionFromDropDown(WebElement element, String text,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info("Fetch option from dropdown from: " + element);
			List<WebElement> allOptions = element.findElements(By.tagName("option"));
			for (WebElement option : allOptions) {
				if (option.getText().equalsIgnoreCase(text)) {
					option.click();
					status = true;
					break;
				}
			}
			if (status) {
				LogPass("Fetched " + text + " From Dropdown", text, driver);
			} else {

				LogFail("Some Exception Occured while fetching from dropdown " + text, text, driver);

			}

		} catch (Exception e) {
			status = false;
			LogFail("Some Exception Occured while fetching from dropdown " + text, text, driver, e);

		}
		return status;
	}

	/**
	 * Method to select drop down value by option using tagName
	 *
	 * @param element Pass the element of dropdown
	 * @param text    Pass the text which need to select from the dropdown
	 * @param driver  Pass the driver
	 * @return
	 */
	public static synchronized boolean fetchOptionFromDropDownonValue(WebElement element, String text,
			RemoteWebDriver driver) {
		boolean status = false;
		try {
			logger.info("Fetch option from dropdown from: " + element);
			List<WebElement> allOptions = element.findElements(By.tagName("option"));
			for (WebElement option : allOptions) {
				if (option.getAttribute("value").equalsIgnoreCase(text)) {
					option.click();
					status = true;
					break;
				}
			}
			if (status) {
				LogPass("Fetched " + text + " From Dropdown", text, driver);
			} else {

				LogFail("Some Exception Occured while fetching from dropdown " + text, text, driver);

			}

		} catch (Exception e) {
			status = false;
			LogFail("Some Exception Occured while fetching from dropdown " + text, text, driver, e);

		}
		return status;
	}

	/**
	 * Method to select drop down value by option using tagName
	 *
	 * @param element Pass the element of dropdown
	 * @param text    Pass the text which need to select from the dropdown
	 * @param driver  Pass the driver
	 * @return
	 */
	public static synchronized String fetchOptionContainsInDropDown(WebElement element, String text,
			RemoteWebDriver driver) {
		String optionName = "NA";
		try {
			logger.info("Fetch option from dropdown from: " + element);
			List<WebElement> allOptions = element.findElements(By.tagName("option"));
			for (WebElement option : allOptions) {

				if (option.getText().contains(text)) {
					optionName = option.getText();
					option.click();
					break;
				}
			}

			LogPass("Fetched " + optionName + " From Dropdown", text, driver);
		} catch (Exception e) {

			LogFail("Some Exception Occured while fetching from dropdown " + optionName, text, driver, e);
		}
		return optionName;
	}

	/**
	 * Method to verify for text in web
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data to compare
	 * @param elementName Pass the element for which you want to check the text
	 * @param driver      Pass the driver
	 * @return
	 */
	public static synchronized boolean checkForText(String storeVar, WebElement element, String data,
			String elementName, RemoteWebDriver driver) {
		boolean status = true;
		try {
			logger.info("Check for text " + data);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				if (actualOutput.trim().replaceAll("\\s", "").equalsIgnoreCase(data.trim().replaceAll("\\s", ""))) {
					logger.info("Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().matches(data.trim().toLowerCase())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);
				} else if (element.getText().trim().equalsIgnoreCase(data.trim())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");

				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

			status = false;
		}
		return status;
	}

	/**
	 * Method to check whether element is present for web
	 *
	 * @param storeVar    Pass the store variable
	 * @param element     Pass the element for which the text need to check
	 * @param elementName Pass the element name for which the text need to check
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForElement(String storeVar, WebElement element, String elementName,
			RemoteWebDriver driver) {
		try {
			logger.info("Check for the element:" + elementName);
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			if (element.isDisplayed()) {
				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is verified successfully");
				LogPass(elementName + "Element is verified successfully", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			}
		} catch (NoSuchElementException e) {
			LogFail("Error occured while checking the element ", elementName, driver, e);

		}
	}

	public static synchronized void verifyElementIsNotDisplayed(WebElement element, String ElementName,
			RemoteWebDriver driver) {
		boolean ElementDisplayed = false;
		try {
			if (element.isDisplayed()) {
				ElementDisplayed = true;
			}

		} catch (Exception ex) {
			ElementDisplayed = false;
			System.out.println(ElementName + "Element not present...");
		}
		if (ElementDisplayed == false) {
			LogPass(ElementName + " Element is not present", ElementName, driver);
		} else {
			LogFail(ElementName + " Element is present", ElementName, driver);

		}
	}

	/**
	 * Method to compare two strings
	 *
	 * @param actual      pass the actual String to compare
	 * @param expected    pass the expected String
	 * @param driver      pass the driver
	 * @param elementName pass the element name for logging purpose
	 */
	public synchronized static void compareString(String actual, String expected, String elementName,
			RemoteWebDriver... driver) {
		try {
			actual = actual == null || actual.equals("") ? "" : actual.replaceAll("\\s+", "");
			expected = expected == null || expected.equals("") ? "" : expected.replaceAll("\\s+", "");
			if (actual.equalsIgnoreCase(expected)) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + expected);
				System.out.println(driver.length);
				if (driver.length > 0)
					LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
							+ expected, elementName, driver[0]);
				else
					LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
							+ expected, elementName);

			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected);
				if (driver.length > 0)
					LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
							+ expected, elementName, driver[0]);
				else
					LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
							+ expected, elementName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogFail("Error occured during the comparision of " + elementName, elementName,
					driver.length > 0 ? driver[0] : null, e);
		}
	}

	/**
	 * Method to compare two strings
	 *
	 * @param actual      pass the actual String to compare
	 * @param expected    pass the expected String
	 * @param driver      pass the driver
	 * @param elementName pass the element name for logging purpose
	 */
	public synchronized static void compareStringContains(String actual, String expected, String elementName,
			RemoteWebDriver... driver) {
		try {
			if (actual.toLowerCase().replaceAll("\\s+", "").contains(expected.toLowerCase().replaceAll("\\s+", ""))) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + expected);
				LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);
			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected);

				LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);

			}
		} catch (Exception e) {
			LogFail("Error occured suring the comparision of " + elementName, elementName,
					driver.length > 0 ? driver[0] : null, e);

		}
	}

	public synchronized static void compareStringContains(String actual, String expected, String elementName) {
		try {
			if (actual.toLowerCase().replaceAll("\\s+", "").contains(expected.toLowerCase().replaceAll("\\s+", ""))) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + expected);
				LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
						+ expected, elementName);
			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected);

				LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
						+ expected, elementName);

			}
		} catch (Exception e) {
			LogFail("Error occured suring the comparision of " + elementName, elementName);

		}
	}

	public synchronized static void compareIntegers(int actual, int expected, String elementName, int range,
			RemoteWebDriver... driver) {
		try {
			int differnce;
			int low;
			int high;
			if (actual > expected) {
				high = actual;
				low = expected;
			} else {
				high = expected;
				low = actual;
			}

			if (range == 0)
				differnce = high - low;
			else
				differnce = ((high - low) / high);

			if (differnce == 0 || Math.abs(differnce) == range) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + expected);

				LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);
			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected);

				LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);
			}
		} catch (Exception e) {
			LogFail("Error occured during the comparision of " + elementName, elementName,
					driver.length > 0 ? driver[0] : null, e);
		}
	}

	public synchronized static void compareDoubles(double actual, double expected, String elementName, int range,
			RemoteWebDriver... driver) {
		try {
			double differnce;
			double low;
			double high;
			if (actual > expected) {
				high = actual;
				low = expected;
			} else {
				high = expected;
				low = actual;
			}

			if (range == 0)
				differnce = high - low;
			else
				differnce = ((high - low) / high);

			if (differnce == 0.0 || Math.abs((int) Math.round(differnce)) == range) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + expected);
				LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);
			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected);
				LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
						+ expected, elementName, driver.length > 0 ? driver[0] : null);
			}
		} catch (Exception e) {

			LogFail("Error occured suring the comparision of " + elementName, elementName,
					driver.length > 0 ? driver[0] : null);
		}
	}

	public synchronized static void compareStringAmongMulitpleData(String actual, ArrayList<String> expected,
			String elementName) {
		try {
			if (expected.contains(actual.trim())) {
				logger.info(
						elementName + " comparision is successful - actual : expected -> " + actual + " : " + actual);
				LogPass(elementName + " comparison Successful ,actual value = " + actual + " expected value = "
						+ actual, elementName);
			} else {
				logger.info(elementName + " comparision is unsuccessful - actual : expected -> " + actual + " : "
						+ expected.toString());

				LogFail(elementName + " comparison unsuccessful ,actual value = " + actual + " expected value = "
						+ expected.toString(), elementName);

			}
		} catch (Exception e) {

			LogFail("Error occured suring the comparision of " + elementName, elementName, e);
		}
	}

	public static synchronized boolean selectValueFromList(List<WebElement> listElement, String exepectedDropDownValue,
			String elementName, RemoteWebDriver driver) {
		boolean status = false;
		try {
			for (int i = 0; i < listElement.size(); i++) {
				if (listElement.get(i).getAttribute("innerText").equalsIgnoreCase(exepectedDropDownValue)) {
					listElement.get(i).click();
					status = true;
					break;
				}
			}

			if (status) {
				LogPass(exepectedDropDownValue + " selected from list", elementName, driver);
			} else {
				LogFail("Error occured during select value from dropdown for " + elementName, elementName, driver);
			}
		} catch (Exception e) {
			status = false;

			LogFail("Error occured during select value from dropdown for " + elementName, elementName, driver, e);

		}
		return status;

	}

	/**
	 * Method to click by Mouse
	 *
	 * @param element pass the element which need to be clicked
	 * @param driver  pass the driver
	 */
	public static synchronized void mouseClick(WebElement element, RemoteWebDriver driver) {
		Actions click = new Actions(driver);
		try {
			logger.info("Mouse click on: " + element);
			click.moveToElement(element).click().perform();
			LogPass("Mouse Click Successful", "", driver);
		} catch (Exception e) {
			LogFail("Mouse Click UnSuccessful", "", driver, e);

		}
	}

	/**
	 * Method to click on element twice
	 *
	 * @param element pass the elemet which need to double clicked
	 * @param driver  pass the driver
	 */
	public static synchronized void doubleClickOnElement(WebElement element, RemoteWebDriver driver) {
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).doubleClick().perform();
			store2db("doubleClickOnElement Successful", "pass");
		} catch (Exception e) {
			LogFail("doubleClickOnElement : Double click failed", "", driver, e);
			throw e;
		}
	}

	/**
	 * Method to move/scroll/view to the element
	 *
	 * @param elementToClick pass the element to be clicked
	 * @param driver         pass the driver
	 */
	public static synchronized void scrollToElement(WebElement elementToClick, RemoteWebDriver driver) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elementToClick);

		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "Error while scrollToElement" + e);
			// getTest().get().fail("scrollToElement: Scroll to Element failed");
			// SQLConnectionHelper.storeintoDB("scrollToElement: Scroll to Element failed",
			// "fail");

			throw e;
		}
	}

	/**
	 * Method to clear browsing data
	 *
	 * @param driver Pass the driver
	 */
	public static void ClearBrowsingData(RemoteWebDriver driver) {
		try {
			logger.info("clearng the browser");
			driver.get("chrome://settings/clearBrowserData");
			Actions action = new Actions(driver);
			Thread.sleep(5000);
			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);

			action.sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(10000);
			logger.info("clearng the browser done");
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "ClearBrowsingData" + e);
		}
	}

	/**
	 * Method to scroll into the view of a web element
	 *
	 * @param elementToClick pass the element which need to scroll and click
	 * @param driver         pass the driver
	 */
	public static synchronized void scrollToWebElement(WebElement elementToClick, RemoteWebDriver driver) {
		Point hoverItem = elementToClick.getLocation();
		((JavascriptExecutor) driver).executeScript("return window.title;");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY() - 400) + ");");
	}

	/**
	 * Method to check value not null
	 *
	 * @param storeVar    pass the store variable
	 * @param element     pass the element for which you want to check the value
	 * @param elementName pass the element name for logging purpose
	 * @param driver      pass the driver
	 */
	public static synchronized void checkValueNotNull(String storeVar, WebElement element, String elementName,
			RemoteWebDriver driver) {
		try {
			logger.info("Check for the element");
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				logger.info("line 3511 --inside the if where storevariable is not null");
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			String textinelement = element.getAttribute("value");
			logger.info("text is " + textinelement);
			if (textinelement != null && !("".equals(textinelement))) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is verified successfully");

				LogPass(elementName + "Element is verified successfully", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the element ", elementName, driver, e);

		}
	}

	/**
	 * Method to check text not null
	 *
	 * @param storeVar    Pass the store variable
	 * @param element     Pass the element for which you want to check the value
	 * @param elementName Pass the element name for logging purpose
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkTextNotNull(String storeVar, WebElement element, String elementName,
			RemoteWebDriver driver) {
		try {
			logger.info("Check for the element");
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				logger.info("line 3511 --inside the if where storevariable is not null");
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			String textinelement = element.getText();
			System.out.println("text is " + textinelement);
			if (textinelement != null && !("".equals(textinelement))) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is verified successfully");

				LogPass(elementName + "Element is verified successfully", elementName, driver);
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is not verified successfully");

				LogFail(elementName + "Element is not verified successfully", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the element ", elementName, driver, e);

		}
	}

	public static synchronized void checkTextNotNull(String storeVar, String elementName, String actualData) {
		try {
			logger.info("checkTextNotNull");
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				storeVariable.put(storeVar, actualData);
			}
			System.out.println("text is " + actualData);
			if (actualData != null && !("".equals(actualData))) {
				logger.info("### The Text is verified successfully");

				LogPass(elementName + "Text verified successfully with '" + actualData + "'", elementName);
			} else {
				logger.info("### The Text is not verified successfully");
				LogFail(elementName + "Text verification unsuccessful with '" + actualData + "'", elementName);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the element ", "", e);

		}
	}

	/**
	 * Method to click on an alert button
	 *
	 * @param confirmationMessage pass the confirmation message
	 * @param elementName         pass the element where need to click
	 * @param driver              pass the driver
	 */
	public static synchronized String clickOnAlert(String confirmationMessage, String elementName,
			RemoteWebDriver driver) {
		String alertText = null;
		try {
			logger.info("Clicking on Alert");
			Alert alert = driver.switchTo().alert();
			if (confirmationMessage.equalsIgnoreCase("Ok")) {
				alert.accept();
				alertText = alert.getText();
				logger.info("Ok Clicked Successfully");

				LogPass(elementName + "Element is Selected successfully", elementName, driver);

			} else if (confirmationMessage.equalsIgnoreCase("Cancel")) {
				alert.dismiss();
				logger.info("Cancel Clicked Successfully");
				LogPass(elementName + "Element is Selected successfully", elementName, driver);

			}
		} catch (Exception e) {
			LogFail("Error occured while clicking the element ", elementName, driver, e);

		}
		return alertText;
	}

	/**
	 * Method to get element middle point
	 *
	 * @param element pass the element for which you need the middle point
	 * @return a String
	 */
	public static String getElementMiddlePoint(WebElement element) {
		int length = 0;
		int height;
		int middleY = 0;
		try {
			logger.info("Get element from Middle point");
			Point point = element.getLocation();
			length = element.getSize().getWidth();
			height = element.getSize().getHeight();
			int getY = point.getY();
			middleY = (int) (getY + height * 1.5) / 2;
		} catch (Exception e) {
			logger.error(
					ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + ProjectConst.ELEMENTNOTFOUNDEXCEPTIONTXT + e);
		}
		return length + "" + "" + "|" + middleY + "";
	}

	/**
	 * Method to clear data
	 *
	 * @param element Pass the element against which the data has to be cleared
	 */
	public synchronized static void clearData(WebElement element) {
		try {
			logger.info("Clearing the log file");
			element.clear();
		} catch (Exception e) {
			logger.info("Error clearing data");
			throw e;
		}
	}

	public static void clearDataUsingRobot() {
		try {
			Robot robot = new Robot();
			Thread.sleep(2000);

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_A);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_A);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_DELETE);
			robot.keyRelease(KeyEvent.VK_DELETE);
		} catch (Exception e) {
			logger.info("Error clearing data");
		}
	}

	public static synchronized String extractDigits(String src) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < src.length(); i++) {
			char c = src.charAt(i);
			if (Character.isDigit(c)) {
				builder.append(c);
			}
		}
		return builder.toString();
	}

	/**
	 * Method to check element is not present for web
	 *
	 * @param elementXpath Pass the elements Xpath
	 * @param elementName  Pass the element name
	 * @param driver       Pass the driver
	 */
	public static synchronized void checkElementNotPresent(String elementXpath, String elementName,
			RemoteWebDriver driver) {
		try {
			logger.info("Check for the element:" + elementName);
			if (!(driver.findElements(By.xpath(elementXpath)).size() > 0)) {

				LogPass(elementName + "Element is not present - successful", elementName, driver);

			} else {
				WebElement element = driver.findElement(By.xpath(elementXpath));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("### The Element is present -  unsuccessful");

				LogFail(elementName + "The Element is present -  unsuccessful", elementName, driver);

				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			}
		} catch (NoSuchElementException e) {

			LogFail("Error occured while checking the element", elementName, driver, e);
			throw e;
		}
	}

	/**
	 * To get the attribute details of the element
	 *
	 * @param element     pass the element
	 * @param elementName pass the element name
	 * @param attribute   get the attribute which we need to take
	 * @param driver      pass the driver
	 * @return returns the value
	 */
	public static String getElementAttributeValue(WebElement element, String elementName, String attribute,
			RemoteWebDriver driver) {
		String status = "false";
		try {
			status = element.getAttribute(attribute);
		} catch (Exception e) {
			logger.error("Exception occured" + e);
			status = "false";
		}
		return status;
	}

	/**
	 * to open a new blank tab
	 * 
	 * @param driver
	 */
	public static synchronized void openNewBlankTab(RemoteWebDriver driver) {
		try {
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		} catch (Exception e) {
			logger.error("Error occured in opening new tab");
		}
	}

	/**
	 * To switch back to parnet tab
	 * 
	 * @param driver
	 */
	public static synchronized void SwitchToParentTab(RemoteWebDriver driver) {
		try {
			ArrayList tabs;
			tabs = new ArrayList(driver.getWindowHandles());
			logger.info(tabs.size() + "");
			driver.switchTo().window((String) tabs.get(0));
		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);

			LogFail("Unable to switch to new tab", "", driver, e);

		}
	}

	public static synchronized void countTabSize(RemoteWebDriver driver) {
		try {
			ArrayList tabs;
			tabs = new ArrayList(driver.getWindowHandles());
			noOfTabs = tabs.size();
		} catch (Exception e) {
			logger.error("Unable to get the tab details " + e);
			e.printStackTrace();

			LogFail("Unable to get the tab details", "", driver, e);
		}
	}

	public static synchronized void switchToNewTab(RemoteWebDriver driver, int tabno) {
		try {
			ArrayList tabs;
			tabs = new ArrayList(driver.getWindowHandles());
			logger.info(tabs.size() + "");
			driver.switchTo().window((String) tabs.get(tabno));
		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();
			LogFail("Unable to switch to new tab", "", driver, e);

		}
	}

	/*
	 * public static synchronized void dragAndDrop(WebElement sourceElement,
	 * WebElement targetElement, String elementName, RemoteWebDriver driver) { try {
	 * Actions a = new Actions(driver); a.dragAndDrop(sourceElement,
	 * targetElement).build().perform();
	 * 
	 * LogPass(elementName + " drag and drop successful", elementName, driver); }
	 * catch (Exception e) { logger.error("Unable to switch to new tab:"+e);
	 * e.printStackTrace();
	 * 
	 * LogFail("Unable to drag and drop", elementName, driver+e);
	 * 
	 * } }
	 */
	/**
	 * 
	 * @param element     element xapath
	 * @param elementName element name
	 * @param xoffset     ex:30
	 * @param yoffset     - ex:0
	 * @param driver
	 */
	public static synchronized void dragAndDrop(WebElement element, String elementName, int xoffset, int yoffset,
			RemoteWebDriver driver) {
		try {
			Thread.sleep(4000);
			Actions action = new Actions(driver);
			Action dragAndDrop = (Action) action.dragAndDropBy(element, xoffset, yoffset).build();
			dragAndDrop.perform();
			Thread.sleep(4000);
			LogPass(elementName + " drag and drop successful", elementName, driver);

		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();

			LogFail("Unable to drag and drop", elementName, driver, e);
		}
	}

	public static synchronized void dragAndDrop(WebElement from, WebElement to, String elementName,
			RemoteWebDriver driver) {
		try {
			Thread.sleep(4000);
			Actions action = new Actions(driver);
			Action dragAndDrop = action.clickAndHold(from).pause(5000).moveToElement(to).pause(2000).release(to)
					.pause(2000).build();
			dragAndDrop.perform();
			Thread.sleep(4000);
			LogPass(elementName + " drag and drop successful", elementName, driver);

		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();

			LogFail("Unable to drag and drop", elementName, driver, e);
		}
	}

	public static synchronized String selectRandomOption4mDropDown(WebElement element, String elementName,
			RemoteWebDriver driver) {
		String select_option = null;
		try {
			Select s = new Select(element);
			int random_int;
			if (s.getOptions().size() >= 5)
				random_int = (int) (Math.random() * (s.getOptions().size() - 2 + 1) + 2);
			else
				random_int = (int) (s.getOptions().size() - 2 + 1);

			Thread.sleep(2000);
			s.selectByIndex(random_int);
			select_option = s.getOptions().get(random_int).getAttribute("innerText");
			LogPass("Selected option " + select_option + " from Dropdown Successfully", elementName, driver);
		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();
			LogFail("selectRandomOption4mDropDown- Random option not Selected", elementName, driver, e);

		}
		return select_option;
	}

	public static synchronized String select4mDropDownByDownArrow(WebElement element, String elementName,
			int numberTimesHitDownArrow, RemoteWebDriver driver) {
		String optionSelected = "";
		try {

			for (int i = 0; i < numberTimesHitDownArrow; i++) {
				element.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(1000);
			}

			element.sendKeys(Keys.RETURN);

			optionSelected = element.getAttribute("value");
			if (optionSelected == null || optionSelected.equalsIgnoreCase(""))
				LogFail("No Option selected from drop down ", elementName, driver);
			else
				LogPass("Selected option " + optionSelected + " from Dropdown Successfully", elementName, driver);
		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			e.printStackTrace();
			LogFail("selectRandomOption4mDropDown- Random option not Selected", elementName, driver, e);

		}
		return optionSelected;
	}

	/**
	 * to add or subtract the days, months,years based on requirement
	 * 
	 * @param ddMMyyyyDate pass the date in dd/MM/yyyy format
	 * @param duration     pass the value to add or subtract from date
	 * @param DMYs         D- days, M- Month, Y - year
	 * @return returns the final date after calculation
	 */
	public static synchronized String addOrSubtractDaysMonthYear(String ddMMyyyyDate, int duration, String DMYs) {
		String newDate = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdfddMMyyyywithSlash.parse(ddMMyyyyDate));

			if (DMYs.equalsIgnoreCase("D")) {
				cal.add(Calendar.DAY_OF_MONTH, duration);
			} else if (DMYs.equalsIgnoreCase("M")) {
				cal.add(Calendar.MONTH, duration);
			} else {
				cal.add(Calendar.YEAR, duration);
			}
			newDate = sdfddMMyyyywithSlash.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured while generating date in getDaysMonthYear " + e);
		}
		return newDate;
	}

	/**
	 * to add or subtract the days, months,years based on requirement
	 * 
	 * @param ddMMyyyyDate pass the date in dd/MM/yyyy format
	 * @param duration     pass the value to add or subtract from date
	 * @param DMYs         D- days, M- Month, Y - year
	 * @return returns the final date after calculation
	 */
	public static synchronized Date addOrSubtractDaysMonthYear(Date date, int duration, String DMYs) {
		Date newDate = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			if (DMYs.equalsIgnoreCase("D")) {
				cal.add(Calendar.DAY_OF_MONTH, duration);
			} else if (DMYs.equalsIgnoreCase("M")) {
				cal.add(Calendar.MONTH, duration);
			} else {
				cal.add(Calendar.YEAR, duration);
			}
			newDate = cal.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured while generating date in getDaysMonthYear " + e);
		}
		return newDate;
	}

	/**
	 * To remove the special charcters from the String
	 * 
	 * @param value pass the string value
	 * @return retruns the string after removing the special characters
	 */
	public static synchronized String removeSpecialCharcters(String value) {
		String result = new String();
		try {
			String[] stringArray = value.split("\\W+");

			for (int i = 0; i < stringArray.length; i++) {
				result = result + " " + stringArray[i];
			}
			System.out.println("Result: " + result);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured while removing special charcter from string " + value + e);
		}
		return result;

	}

	public static synchronized String removeSpecialCharctersUsingRegex(String value) {
		String result = "";
		try {
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match = pt.matcher(value);
			while (match.find()) {
				result = match.group();
			}
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error occured while removing special charcter from string " + value + e);
		}
		return result;

	}

	/**
	 * To remove the first and last characters from the string
	 * 
	 * @param value         pass the string value
	 * @param FLB           pass f-first l- last b- both sides
	 * @param noOfCharcters pass the no of characters to remove
	 * @return returns after removing the characters from the string
	 */

	public static synchronized String removeFirstOrLastCharacters(String value, String FLB, int noOfCharcters) {
		String result = "";
		try {
			if (FLB.equalsIgnoreCase("L"))
				result = value.substring(0, value.length() - noOfCharcters);
			else if (FLB.equalsIgnoreCase("F"))
				result = value.substring(noOfCharcters, value.length());
			else {
				result = value.substring(0, value.length() - noOfCharcters);
				result = result.substring(noOfCharcters, result.length());
			}
			System.out.println("Result: " + result);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured while removing special charcter from string " + value + e);
		}
		return result;

	}

	public static synchronized void rightClickOnElement(WebElement element, String elementName,
			RemoteWebDriver driver) {
		try {
			Actions actions = new Actions(driver);
			actions.contextClick(element).perform();
			Thread.sleep(3000);

			LogPass("Right click on " + elementName + " successful", elementName, driver);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured while right clicking on element " + e);
			LogFail("Right click on " + elementName + " unsuccessful", elementName, driver, e);
		}
	}

	public static File ReadLatestFile(String fileLocation, String... extension) {
		File f1 = null;
		try {
			File folder = new File(fileLocation);
			System.out.println(folder);
			File[] listOfFiles;
			if (extension.length > 0) {
				FileFilter fileFilter = new WildcardFileFilter("*." + extension[0]);
				listOfFiles = folder.listFiles(fileFilter);
			} else {
				listOfFiles = folder.listFiles();

			}
			File lastModifiedFile;
			if (listOfFiles.length == 1) {
				lastModifiedFile = listOfFiles[0];
			} else {
				System.out.println(listOfFiles[0]);
				lastModifiedFile = listOfFiles[0];
				for (int i = 1; i < listOfFiles.length; i++) {

					if (lastModifiedFile.lastModified() < listOfFiles[i].lastModified()) {
						lastModifiedFile = listOfFiles[i];
					}

				}
			}
			f1 = new File(fileLocation + MainUtil.file_separator + lastModifiedFile.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return f1;
	}

	public static File renameFile(String fileName, File... f1) {
		File FilePath = null;
		try {

			if (f1.length > 0) {
				FilePath = f1[0];
			} else {
				FilePath = MainUtil.ReadLatestFile(MainUtil.downlaodfilepath);
				System.out.println(FilePath.getAbsolutePath());
			}

			System.out.println(FilePath.getName());
			String newName = FilePath.getAbsolutePath().replace(FilePath.getName(), fileName);

			File oldfile = new File(newName);
			if (oldfile.exists()) {
				System.out.println("File exists");
				boolean b = oldfile.renameTo(new File(newName + "_Old"));
				if (b) {
					System.out.println("File Renamed Successfully to old...");
				}

			} else {
				System.out.println("File does not exists");

			}
			boolean b = FilePath.renameTo(new File(newName));
			if (b) {
				System.out.println("File Renamed Successfully...");
			}

			FilePath = new File(newName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FilePath;
	}

	public static synchronized void closeTab(RemoteWebDriver driver, int tabno) {
		try {
			ArrayList tabs;
			tabs = new ArrayList(driver.getWindowHandles());
			logger.info(tabs.size() + "");
			driver.switchTo().window((String) tabs.get(tabno)).close();
		} catch (Exception e) {
			logger.error("Unable to switch to new tab:" + e);
			// getTest().get().fail("Unable to switch to new tab",
			// ExtentScreenCapture.captureSrceenFail("switchToNewTab", driver));
		}
	}

	public static synchronized void scrollToTop(RemoteWebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "Error while scrollToTop" + e);
			throw e;
		}
	}

	/**
	 * This method is used to scroll with in the area in web
	 * 
	 * @param areaElement      pass the xpath of the div area where u want to scroll
	 * @param length_direction mention the legth u want to scroll and + down - for
	 *                         upward direction - 0 - for top
	 * @param driver           driver session
	 */
	public static synchronized void scrollUDbyArea(WebElement areaElement, int length_direction,
			RemoteWebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollTop = arguments[1];", areaElement, length_direction);

		} catch (Exception e) {
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "Error while scrollUDbyArea" + e);
			LogFail("scrollUDbyArea:  Scroll in area failed", "", driver);
			throw e;
		}
	}

	public static synchronized void checkForTextBasedOnAttribute(String storeVar, WebElement element, String data,
			String attributeName, String elementName, RemoteWebDriver driver) {
		try {
			logger.info("Check for text " + data);
			String actualOutput = element.getAttribute(attributeName);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {

				scrollToElement(element, driver);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				if (actualOutput.replace("\n", "").replace("\r", "").trim().equals(data.trim())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);
				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
						"border: 5px solid red;");
				logger.info("#### The Element is not found & Unable to verify Text");
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: none;");

				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

			}
		} catch (Exception e) {
			logger.error("#### Error occured while checking the text in the " + elementName);
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "checkForText" + e);
			LogFail("Error occured while checking the text in the " + elementName + " Expected: " + data, elementName,
					driver, e);
		}
	}

	public static synchronized int convertExcelToImage(String ExcelFilePathName, String ImageName,
			String... sheetName) {
		int totalNoOfImages = 0;
		try {

			// Load Excel file
			Workbook workbook = new Workbook(ExcelFilePathName);

			// Create an object of ImageOrPrintOptions
			ImageOrPrintOptions imgOptions = new ImageOrPrintOptions();

			// Set the output image type
			imgOptions.setImageType(ImageType.PNG);

			// Get the first worksheet
			Worksheet sheet;
			if (sheetName.length > 0)
				sheet = workbook.getWorksheets().get(sheetName[0]);
			else
				sheet = workbook.getWorksheets().get(0);

			// Create a SheetRender object for the target sheet
			SheetRender sr = new SheetRender(sheet, imgOptions);
			totalNoOfImages = sr.getPageCount();
			System.out.println(totalNoOfImages);
			for (int page = 0; page < sr.getPageCount(); page++) {
				// Generate an image for the worksheet

				sr.toImage(page, ExtentTestNGITestListener.passLocation + MainUtil.file_separator + ImageName + "_"
						+ page + ".png");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return totalNoOfImages;
	}

	/**
	 * Round the decimal to four digit after decimal point
	 * 
	 * @param amount pass the amount
	 * @return returns the rounded to four decimal point number
	 */
	public static double roundingto4decimal(double amount) {
		double rounding = Math.round(amount * Math.pow(10, (double) 4)) / Math.pow(10, (double) 4);
		return rounding;
	}

	/**
	 * Round the decimal to three digit after decimal point
	 * 
	 * @param amount pass the amount
	 * @return returns the rounded to three decimal point number
	 */
	public static double roundingto3decimal(double amount) {
		double rounding = Math.round(amount * Math.pow(10, (double) 3)) / Math.pow(10, (double) 3);
		return rounding;
	}

	/**
	 * Round the decimal to two digit after decimal point
	 * 
	 * @param amount pass the amount
	 * @return returns the rounded to two decimal point number
	 */
	public static double roundingto2decimal(double amount) {

		double rounding = Math.round(amount * 100d) / 100d;
		return rounding;
	}

	public static void uploadFileRobotFrameWork(String filepath) throws AWTException, InterruptedException {
		try {
			Robot robot = new Robot();
			Thread.sleep(2000);

			StringSelection str = new StringSelection(filepath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void assertFail(String failureMessage, RemoteWebDriver driver, Exception e,
			boolean... isPageRefresh) {
		try {
			LogFail(failureMessage, "", driver, e);
			if (isPageRefresh.length > 0 && isPageRefresh[0] == true) {
				driver.navigate().refresh();
			}
			MainUtil.storeVariable.put("TEST_FAIL", "true");
			Assert.fail(failureMessage);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public static void cleanDirectory(String directoryPath) {
		try {
			FileUtils.cleanDirectory(new File(directoryPath));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void delete4mDirectory(File file, int days, String directoryName) {
		for (File subfile : file.listFiles()) {
			try {
				long d1 = MainUtil.addOrSubtractDaysMonthYear(new Date(), -30, "D").getTime();
				System.out.println(d1);
				long d2 = subfile.lastModified();
				long difference_In_Time = d1 - d2;
				System.out.println(subfile.lastModified() < difference_In_Time);
				difference_In_Time = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
				System.out.println(difference_In_Time);
				System.out.println(subfile.lastModified());
				System.out.println(subfile.getName());

				if (subfile.isDirectory() && Math.abs(difference_In_Time) >= 30) {
					if (directoryName.equalsIgnoreCase("")) {
						File[] listFiles = subfile.listFiles();
						for (File listFile : listFiles) {
							if (Math.abs(difference_In_Time) >= 30) {
								if (!listFile.delete()) {
									System.err.println("Unable to delete file: " + listFile);
								}
							}
						}
					} else {
						if (subfile.getName().startsWith(directoryName) && Math.abs(difference_In_Time) >= 30) {
							FileUtils.deleteDirectory(subfile);
						}

					}
				}
			} catch (Exception e) {
				System.out.println("No folders exists");
				// e.printStackTrace();
			}
		}
	}

	/**
	 * check the element contains text by removing special character
	 * 
	 * @param storeVar
	 * @param element
	 * @param data
	 * @param elementName
	 * @param driver
	 * @return
	 */
	public static synchronized boolean checkForContainsTextremovingSpcCharc(String storeVar, WebElement element,
			String data, String elementName, RemoteWebDriver driver) {
		boolean status = true;
		try {
			logger.info("Contains Check for text " + data);
			String actualOutput = element.getText();
			actualOutput = removeSpecialCharcters(actualOutput);
			data = removeSpecialCharcters(data);
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Contains Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (element.getText().trim().contains(data.trim())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (actualOutput.trim().replaceAll("\\s", "").contains(data.trim().replaceAll("\\s", ""))) {
					logger.info("Contains Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
					logger.info("####Contains Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

					status = false;
				}
			} else {

				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

				status = false;
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver);
			status = false;
		}
		return status;
	}

	public static void main(String[] args) {
		File fileLocation = ReadLatestFile(System.getProperty("user.dir") + MainUtil.file_separator + "src"
				+ MainUtil.file_separator + "main" + MainUtil.file_separator + "resources" + MainUtil.file_separator
				+ "files" + MainUtil.file_separator, "xlsx");
		System.out.println(fileLocation.toString());
	}

	public static void checkArrayListContainsText(String expected, ArrayList<String> arrayDetails, String elementName) {
		try {
			System.out.println(arrayDetails == null ? "" : arrayDetails.toString());
			if (arrayDetails.toString().contains(expected.trim())) {
				logger.info(expected + " found - actual : expected -> " + arrayDetails.toString() + " : " + expected);
				LogPass(expected + " found - actual  -> " + expected + " : expected -> " + expected, elementName);
			} else {
				logger.info(
						expected + " not found - actual : expected -> " + arrayDetails.toString() + " : " + expected);

				LogFail(expected + " not found - actual  -> " + arrayDetails.toString() + " : expected ->" + expected,
						elementName);

			}
		} catch (Exception e) {
			LogFail("Error occured while comparing the text in the arraylist" + arrayDetails.toString(), elementName,
					e);
		}
	}

	public static void checkArrayListDoesnotContainsText(String expected, ArrayList<String> arrayDetails,
			String elementName) {
		try {
			System.out.println(arrayDetails == null ? "" : arrayDetails.toString());
			if (!arrayDetails.toString().contains(expected.trim())) {
				logger.info(
						expected + " not found - actual : expected -> " + arrayDetails.toString() + " : " + expected);
				LogPass(expected + " not found - actual  -> " + expected + " : expected -> " + expected, elementName);
			} else {
				logger.info(expected + "  found - actual : expected -> " + arrayDetails.toString() + " : " + expected);

				LogFail(expected + "  found - actual  -> " + arrayDetails.toString() + " : expected ->" + expected,
						elementName);

			}
		} catch (Exception e) {
			LogFail("Error occured while comparing the text in the arraylist" + arrayDetails.toString(), elementName,
					e);
		}
	}

}
