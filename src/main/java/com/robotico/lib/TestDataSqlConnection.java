package com.robotico.lib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;

public class TestDataSqlConnection {

	/**
	 * Read testdata from testdata_scenario table based on scenario
	 * 
	 * @param scenario
	 * @param tableName
	 * @return
	 */
	public static HashMap<String, String> read4mTestDataScenarioTable(String scenario, String tableName) {
		System.out.println("Reached here in read4mTestDataScenarioTable");

		HashMap<String, String> data = new HashMap<String, String>();
			try (Connection conn = SQLConnectionHelper.getFrothShcemaDBConnection()) {
				String strQuery;
				strQuery = "SELECT * FROM " + tableName + " where Scenario = ? ";

				try (PreparedStatement preparedStatement = conn.prepareStatement(strQuery)) {
					preparedStatement.setString(1, scenario.replaceAll("\\s+", " "));

					try (ResultSet recordset = preparedStatement.executeQuery()) {
						ResultSetMetaData rsmd;
						while (recordset.next()) {
							rsmd = recordset.getMetaData();
							for (int i = 1; i < rsmd.getColumnCount(); i++) {
								data.put(rsmd.getColumnName(i), recordset.getString(i));
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return data;
	}

	public static HashMap<String, String> getSiteDetails() throws Exception {

		HashMap<String, String> data = new HashMap<String, String>();
			try (Connection conn = SQLConnectionHelper.getFrothShcemaDBConnection()) {
				String strQuery;

				strQuery = "SELECT * FROM sites where id=? ";

				try (PreparedStatement preparedStatement = conn.prepareStatement(strQuery)) {
					preparedStatement.setString(1, MainUtil.SITE_ID);

					try (ResultSet recordset = preparedStatement.executeQuery()) {
						ResultSetMetaData rsmd;
						while (recordset.next()) {
							rsmd = recordset.getMetaData();
							for (int i = 1; i < rsmd.getColumnCount(); i++) {
								data.put(rsmd.getColumnName(i), recordset.getString(i));
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		return data;

	}

	public static void setsitedetails() {
		try {
			if (MainUtil.SITE_ID.equalsIgnoreCase("NA")) {

				MainUtil.siteDetails.put("tenant_url", System.getProperty("URL"));
				MainUtil.siteDetails.put("tenant_username", System.getProperty("tenant_username"));
				MainUtil.siteDetails.put("tenant_password", System.getProperty("tenant_password"));
				MainUtil.siteDetails.put("tenant_security", System.getProperty("tenant_security"));
				System.out.println(MainUtil.siteDetails.toString());
			} else
				MainUtil.siteDetails = getSiteDetails();

			System.out.println(MainUtil.siteDetails.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
