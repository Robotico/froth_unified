package com.robotico.lib;

import java.time.Duration;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;

import com.robotico.lib.MainUtil.ProjectConst;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class MobileUtils extends WindowsUtils {
	private static Logger logger = LogManager.getLogger(MobileUtils.class);

	/**
	 * Method to verify for contains text in android
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForContainsText(String storeVar, WebElement element, String data,
			String elementName, AppiumDriver driver) {
		try {
			logger.info("Check for contains text in: " + element);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info(new StringBuilder("### Check for contains Text: Target element verification first:")
					.append(element).toString());
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").contains(data.trim().replaceAll("\\s", ""))) {
					logger.info("Contains Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().contains(data.trim())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else {
					logger.info("#### Contains Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to verify for starts with text in android
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForStartsWithText(String storeVar, WebElement element, String data,
			String elementName, AppiumDriver driver) {
		try {
			logger.info("Check for starts with text in: " + element);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info(new StringBuilder("### Check for starts with Text: Target element verification first:")
					.append(element).toString());
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").startsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("StartsWith Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().startsWith(data.trim().toLowerCase())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().startsWith(data.trim())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### StartsWith Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail("The Element is not found & Unable to verify Text", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver);

		}
	}

	/**
	 * Method to verify for StartsWith text in iOS
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForStartsWithText(String storeVar, WebElement element, String data,
			String elementName, IOSDriver driver) {
		try {
			logger.info("Check for StartsWith text in the element: " + elementName);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").startsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("StartsWith Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().startsWith(data.trim().toLowerCase())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().startsWith(data.trim())) {
					logger.info("#### StartsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver);
		}
	}

	/**
	 * Method to verify for EndsWith text in android
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForEndsWithText(String storeVar, WebElement element, String data,
			String elementName, AppiumDriver driver) {
		try {
			logger.info("Check for ends with text in: " + element);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info(new StringBuilder("### Check for ends with Text: Target element verification first:")
					.append(element).toString());
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").endsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("endsWith Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().endsWith(data.trim().toLowerCase())) {
					logger.info("#### EndsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().endsWith(data.trim())) {
					logger.info("#### EndsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);
				} else {
					logger.info("#### EndsWith Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to verify for EndsWith text in iOS
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForEndsWithText(String storeVar, WebElement element, String data,
			String elementName, IOSDriver driver) {
		try {
			logger.info("Check for EndsWith text in the element: " + elementName);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").endsWith(data.trim().replaceAll("\\s", ""))) {
					logger.info("EndsWith Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().endsWith(data.trim().toLowerCase())) {
					logger.info("#### EndsWith Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().endsWith(data.trim())) {
					logger.info("#### EndsWith Text Found in the Target location ####");

					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to verify for text in android
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForText(String storeVar, WebElement element, String data, String elementName,
			AppiumDriver driver) {
		try {
			logger.info("Check for text in: " + element);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info(new StringBuilder("### Check for Text: Target element verification first:").append(element)
					.toString());
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").equalsIgnoreCase(data.trim().replaceAll("\\s", ""))) {
					logger.info("Text matches with the given text");

					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().matches(data.trim().toLowerCase())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().equalsIgnoreCase(data.trim())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to check whether element is present for android (mobile)
	 *
	 * @param storeVar    Pass the store variable
	 * @param element     Pass the element for which need to check the element exist
	 *                    or not
	 * @param elementName Pass the element name for which need to check the element
	 *                    exist or not
	 * @param driver      Pass the Android driver
	 */
	public static synchronized void checkForElement(String storeVar, WebElement element, String elementName,
			AppiumDriver driver) {
		try {
			logger.info("Check for the element: " + elementName);
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				logger.info("line 3511 --inside the if where storevariable is not null");
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			if (element.isDisplayed()) {
				logger.info("### The Element is verified successfully");
				LogPass(elementName + "Element is verified successfully", elementName, driver);
			} else {
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the element ", elementName, driver, e);
		}
	}

	/**
	 * Method to scroll/swipe Up Down left right in mobile
	 *
	 * @param driver    Pass the instance of mobile driver
	 * @param duration  Pass the time for which duration you need to scroll
	 * @param direction Pass the direction
	 */
	public static synchronized void scrollUDLR(MobileDriver driver, int duration, String direction) {
		try {
			logger.info("Scrolling UDLR");
			Thread.sleep(1000);
			Dimension size = driver.manage().window().getSize();
			logger.info("size ---" + size + "::::;" + size.height + ":::::" + size.width);
			TouchAction action = new TouchAction(driver);
			int starty = 0;
			int endy = 0;
			int startx = 0;
			switch (direction) {
			case "R":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.70);
				startx = size.getWidth() / 2;
				break;
			case "L":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.20);
				startx = size.getWidth() / 2;
				break;
			case "U":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.height * 0.20);
				startx = size.getWidth() / 2;
				break;
			case "D":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.height * 0.70);
				startx = size.getWidth() / 2;
				break;
			}
			logger.info(startx + "::::::" + starty + "::::::" + endy + "dfugv");
			action.press(PointOption.point(startx, starty))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
					.moveTo(PointOption.point(startx, endy)).release().perform();
		} catch (Exception e) {
			logger.info("Not Scrolled");
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + "\n"
					+ ProjectConst.ELEMENTNOTFOUNDEXCEPTIONTXT.getMsg() + e);
		}
	}

	/**
	 * Method to scroll/swipe Up Down left right in mobile
	 *
	 * @param driver    Pass the instance of mobile driver
	 * @param duration  Pass the time for which duration you need to scroll
	 * @param direction Pass the direction
	 */
	public static synchronized void scrollUDLRShort(MobileDriver driver, int duration, String direction) {
		try {
			logger.info("Scrolling UDLR");
			Thread.sleep(1000);
			Dimension size = driver.manage().window().getSize();
			TouchAction action = new TouchAction(driver);
			int starty = 0;
			int endy = 0;
			int startx = 0;
			switch (direction) {
			case "R":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.60);
				startx = size.getWidth() / 2;
				break;
			case "L":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.30);
				startx = size.getWidth() / 2;
				break;
			case "U":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.height * 0.30);
				startx = size.getWidth() / 2;
				break;
			case "D":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.height * 0.60);
				startx = size.getWidth() / 2;
				break;
			}
			action.press(PointOption.point(startx, starty))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
					.moveTo(PointOption.point(startx, endy)).release().perform();
		} catch (Exception e) {
			logger.info("Not Scrolled");
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + "\n"
					+ ProjectConst.ELEMENTNOTFOUNDEXCEPTIONTXT.getMsg() + e);
		}
	}

	/**
	 * Method to scroll/swipe Up Down left right in mobile
	 *
	 * @param driver    Pass the instance of mobile driver
	 * @param duration  Pass the time for which duration you need to scroll
	 * @param direction Pass the direction
	 */
	public static synchronized void scrollUDLRLong(MobileDriver driver, int duration, String direction) {
		try {
			logger.info("Scrolling UDLR");
			Thread.sleep(1000);
			Dimension size = driver.manage().window().getSize();
			TouchAction action = new TouchAction(driver);
			int starty = 0;
			int endy = 0;
			int startx = 0;
			switch (direction) {
			case "R":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.60);
				startx = size.getWidth() / 2;
				break;
			case "L":
				starty = (int) (size.height * 0.50);
				endy = (int) (size.width * 0.30);
				startx = size.getWidth() / 2;
				break;
			case "U":
				starty = (int) (size.height * 0.80);
				endy = (int) (size.height * 0.30);
				startx = size.getWidth() / 2;
				break;
			case "D":
				starty = (int) (size.height * 0.30);
				endy = (int) (size.height * 0.80);
				startx = size.getWidth() / 2;
				break;
			}
			action.press(PointOption.point(startx, starty))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
					.moveTo(PointOption.point(startx, endy)).release().perform();
		} catch (Exception e) {
			logger.info("Not Scrolled");
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + "\n"
					+ ProjectConst.ELEMENTNOTFOUNDEXCEPTIONTXT.getMsg() + e);
		}
	}

	/**
	 * Mobile scroll for using JavaScript
	 *
	 * @param driver
	 * @param element
	 */
	public static synchronized void jsMobileScroll(MobileDriver driver, MobileElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		scrollObject.put("element", ((RemoteWebElement) element).getId());
		js.executeScript("mobile: scroll", scrollObject);
	}

	/**
	 * Method to hide keyboard in mobile
	 *
	 * @param driver pass the driver
	 */
	public static synchronized void hideKeyboard(MobileDriver driver) {
		try {
			logger.info("Hide the keyboard");
			driver.hideKeyboard();
		} catch (Exception e) {
			logger.error("Exception occured while hiding the Keyboard:");
		}
	}

	/**
	 * Method to verify for Contains text in iOS
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForContainsText(String storeVar, WebElement element, String data,
			String elementName, IOSDriver driver) {
		try {
			logger.info("Check for Contains text in the element: " + elementName);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").contains(data.trim().replaceAll("\\s", ""))) {
					logger.info("Contains Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().contains(data.trim())) {
					logger.info("#### Contains Text Found in the Target location ####");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");
				LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
						+ actualOutput, elementName, driver);
			}
		} catch (Exception e) {
			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to verify for text in iOS
	 *
	 * @param storeVar    Pass the storevariable
	 * @param element     Pass the element for which you want to check the text
	 * @param data        Pass the data need to compare
	 * @param elementName Pass the element name for which you want to check the text
	 * @param driver      Pass the driver
	 */
	public static synchronized void checkForText(String storeVar, WebElement element, String data, String elementName,
			IOSDriver driver) {
		try {
			logger.info("Check for text in the element: " + elementName);
			String actualOutput = element.getText();
			if (!(storeVar.equalsIgnoreCase(""))) {
				storeVariable.put(storeVar, actualOutput);
			}
			logger.info("### Check for Text: Target element verification first:" + element);
			if (element.isDisplayed()) {
				if (actualOutput.trim().replaceAll("\\s", "").equalsIgnoreCase(data.trim().replaceAll("\\s", ""))) {
					logger.info("Text matches with the given text");
					LogPass("Text matches! " + elementName + " - Expected: " + data + "\n - Actual: " + actualOutput,
							elementName, driver);

				} else if (element.getText().trim().toLowerCase().matches(data.trim().toLowerCase())) {
					logger.info("#### Text Found in the Target location ####");
					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else if (element.getText().trim().equalsIgnoreCase(data.trim())) {
					logger.info("#### Text Found in the Target location ####");

					LogPass(elementName + " Text Found in the Target location", elementName, driver);

				} else {
					logger.info("#### Text Not found in the Target location ####");
					LogFail("Text does not matches " + elementName + " - Expected: " + data + "\n - Actual: "
							+ actualOutput, elementName, driver);

				}
			} else {
				logger.info("#### The Element is not found & Unable to verify Text");

				LogFail(elementName + "The Element is not found & Unable to verify Text", elementName, driver);

			}
		} catch (Exception e) {

			LogFail("Error occured while checking the text in the " + elementName, elementName, driver, e);

		}
	}

	/**
	 * Method to check whether element is present for android (mobile)
	 *
	 * @param storeVar    Pass the store variable
	 * @param element     Pass the element for which need to check the element exist
	 *                    or not
	 * @param elementName Pass the element name for which need to check the element
	 *                    exist or not
	 * @param driver      Pass the IOS driver
	 */
	public static synchronized void checkForElement(String storeVar, WebElement element, String elementName,
			IOSDriver driver) {
		try {
			logger.info("Check for the element");
			if (!(storeVar.equalsIgnoreCase("") || storeVar == null)) {
				logger.info("line 3511 --inside the if where storevariable is not null");
				storeVariable.put(storeVar, element.getText());
				logger.info("### Check for Text: Target element verification first:" + element);
				logger.info("### @@@ The element to be found:" + element);
			}
			if (element.isDisplayed()) {
				logger.info("### The Element is verified successfully");
				LogPass(elementName + "Element is verified successfully", elementName, driver);
			} else {
				logger.info("### The Element is not verified successfully");
				LogFail(elementName + "Element is not verified successfully", elementName, driver);

			}
		} catch (Exception e) {

			LogFail("Error occured while checking the element ", elementName, driver, e);

		}
	}

}
