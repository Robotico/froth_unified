package com.robotico.lib;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FileTransfer {

	public static void transferGeneratedFileToRESFTP(String filePath, String sftpPath, Channel channel) {
		try {

			System.out.println("File path ====>" + filePath);
			System.out.println("Destination file path =====>" + sftpPath);
			ChannelSftp channelSftp = (ChannelSftp) channel;
			try {
				channelSftp.cd(sftpPath);
			} catch (SftpException e) {
				channelSftp.mkdir(sftpPath);
				channelSftp.cd(sftpPath);
			}
			File f2 = new File(filePath);
			channelSftp.put(new FileInputStream(f2), f2.getName(), 0);
			System.out.println("Generated" + filePath + " File Moved Successfully to  SFTP location");
			channelSftp.exit();
		} catch (Exception ex) {
			System.out.println("Generated" + filePath + " File failed to move to RE SFTP Location");
			ex.printStackTrace();
		}
	}

	public static ChannelSftp connecttosftp(String ipaddress, String username, String password, int port) {
		JSch jsch = new JSch();
		ChannelSftp channel = null;
		try {

			Session session = jsch.getSession(username, ipaddress, port);

			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			config.put("PreferredAuthentications", "password");
			session.setConfig(config);

			session.setPassword(password);
			session.connect();
			System.out.println("Host connected...");

			channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			System.out.println("sftp channel opened and connected.");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return channel;
	}
}
