package com.robotico.lib;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.markuputils.Markup;
import com.robotico.lib.MainUtil.ProjectConst;
import com.robotico.listener.ExtentScreenCapture;
import com.robotico.listener.ExtentTestNGITestListener;

public class basicUtils {
	private static Logger logger = LogManager.getLogger(basicUtils.class);

	public static String ss = System.getProperty("ss") == null || System.getProperty("ss").equalsIgnoreCase("true")
			? "true"
			: System.getProperty("ss");
	public static String db = System.getProperty("db") == null || System.getProperty("db").equalsIgnoreCase("true")
			? "true"
			: System.getProperty("db");

	public static String USER_DIR = System.getProperty("user.dir");
	public static Map<String, String> storeVariable = new HashMap<>();
	public static String testcaseName;
	public static String parentWin = null;
	public static String APPLICATION_NAME;
	public static String Image_Path;
	public static String ENVIRONMENT = System.getProperty("env");
	public static String vm_no = System.getProperty("vm_no") == null ? "VM1" : System.getProperty("vm_no");

	public static String HubUrl;
	public static String MCMCREMARKS;

	public static SimpleDateFormat sdfyyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
	public static SimpleDateFormat sdfddMMyyyywithHypen = new SimpleDateFormat("dd-MM-yyyy");
	public static SimpleDateFormat sdfyyyyMMddwithHypen = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat sdfMMddyyyywithSlash = new SimpleDateFormat("MM/dd/yyyy");
	public static SimpleDateFormat sdfddMMyyyywithSlash = new SimpleDateFormat("dd/MM/yyyy");
	public static SimpleDateFormat sdfdMMMyyyywithSlash = new SimpleDateFormat("d/MMM/yyyy");
	public static SimpleDateFormat sdfdMmmyyyywithSlash = new SimpleDateFormat("d/Mmm/yyyy");
	public static SimpleDateFormat sdfddMMyywithSlash = new SimpleDateFormat("dd/MM/yy");
	public static SimpleDateFormat sdfdMyyyywithSlash = new SimpleDateFormat("d/M/yyyy");
	public static SimpleDateFormat sdfMdyyyywithSlash = new SimpleDateFormat("M/d/yyyy");
	public static SimpleDateFormat sdfyyyyMMddThhmmssdotssshhmmwithhypen = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss");
	public static SimpleDateFormat sdfyyyyMMddhhmmsswithhypen = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static SimpleDateFormat sdfMMM = new SimpleDateFormat("MMM");
	public static SimpleDateFormat sdfyyyy = new SimpleDateFormat("yyyy");

	public static SimpleDateFormat sdfd = new SimpleDateFormat("d");
	public static String downlaodfilepath;

	public static DecimalFormat df2 = new DecimalFormat("0.00");
	public static DecimalFormat df3 = new DecimalFormat("0.000");
	public static DecimalFormat df4 = new DecimalFormat("0.0000");

	public static String SITE_ID = System.getProperty("siteid") == null
			|| System.getProperty("siteid").equalsIgnoreCase("") ? "NA" : System.getProperty("siteid");
	public static String file_separator = System.getProperty("file.separator");
	static Markup m;
	public static int noOfTabs;

	public static HashMap<String, String> scenarioDetails = new HashMap<>();
	public static HashMap<String, String> siteDetails = new HashMap<>();

	public static void store2db(String comment, String PF) {
		if (!db.equalsIgnoreCase("false"))
			SQLConnectionHelper.storeTestResultsInDB(comment, PF);
	}

	public static void LogInfo(String comment, String elementName, RemoteWebDriver... driver) {
		try {
			if (ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().info(elementName + " - " + comment);
			else if (ss.equalsIgnoreCase("true")) {
				if (driver.length > 0)
					ExtentTestNGITestListener.getTest().get().info(elementName + " - " + comment,
							ExtentScreenCapture.captureSrceenPass(elementName, driver[0]));
				else
					ExtentTestNGITestListener.getTest().get().info(elementName + " - " + comment);
			} else
				ExtentTestNGITestListener.getTest().get().info(elementName + " - " + comment);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void LogPass(String comment, String elementName, RemoteWebDriver... driver) {
		try {
			if (ss.equalsIgnoreCase("true")) {
				System.out.println(driver.length);
				if (driver.length > 0)
					ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment,
							ExtentScreenCapture.captureSrceenPass(elementName, driver[0]));
				else
					ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);

			} else if (ss.equalsIgnoreCase("fail") || ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);
			else
				ExtentTestNGITestListener.getTest().get().pass(elementName + " - " + comment);

			store2db(comment, "pass");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void LogFail(String comment, String elementName, RemoteWebDriver driver, Exception... e) {
		try {
			logger.error(comment);
			logger.error(ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + (e.length > 0 ? e[0] : ""));
			if (e != null && e.length > 0)
				e[0].printStackTrace();

			if (ss.equalsIgnoreCase("true") || ss.equalsIgnoreCase("fail")) {

				if (driver == null)
					ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment);
				else
					ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment,
							ExtentScreenCapture.captureSrceenFail(elementName, driver));

			} else if (ss.equalsIgnoreCase("false"))
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment);
			else
				ExtentTestNGITestListener.getTest().get().fail(elementName + " - " + comment);

			store2db(comment, "fail");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
