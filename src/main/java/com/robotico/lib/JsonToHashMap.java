package com.robotico.lib;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonToHashMap {
	private static Properties configprop = null;

	public static HashMap<String, String> readJsonAndStoreInMap(String Scenario) {
		JSONParser parser = new JSONParser();
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, String> studentMapData = new HashMap<String, String>();
		try {

			Object obj = parser.parse(new InputStreamReader(
					new FileInputStream(MainUtil.storeVariable.get("FILE_PATH_LOCATION") == null ? "."
							: MainUtil.storeVariable.get("FILE_PATH_LOCATION") + "\\JsonFiles\\" + Scenario + ".json"),
					StandardCharsets.UTF_8));

			/*
			 * Object obj = parser.parse(new InputStreamReader(new
			 * FileInputStream(".\\JsonFiles\\" + Scenario + ".json")));
			 */
			// Object obj = parser.parse(new FileReader(".\\JsonFiles\\" + Scenario +
			// ".json"));
			if (obj == null)
				System.out.println("File not found");
			JSONObject jsonObject = (JSONObject) obj;
			System.out.println(jsonObject.size());
			Iterator<String> keysItr = jsonObject.keySet().iterator();
			while (keysItr.hasNext()) {
				String key = keysItr.next();
				String expectedValue = new String(((String) jsonObject.get(key)).getBytes("UTF-8"), "UTF-8");
				studentMapData.put(key, expectedValue);
			}
			// studentMapData = mapper.readValue(jsonObject.toString(), HashMap.class);
			// System.out.println(studentMapData.toString());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("*************************** No json file " + Scenario + " found");
			// TODO: handle exception
		}
		return studentMapData;

	}

	public static void main(String[] args) {
		readJsonAndStoreInMap("ie");
	}

	public static String getProperties(String Scenario, String propertyName) {
		System.out.println("Iside to read porperty files" + Scenario + " " + propertyName);
		if (configprop == null) {
			try {
				InputStream in = new FileInputStream(".\\JsonFiles\\" + Scenario + ".properties");
				configprop = new Properties();
				configprop.load(in);
				configprop.getProperty(propertyName);
				System.out.println("propertyName=" + propertyName);
				System.out.println("configprop.getProperty(propertyName);" + configprop.getProperty(propertyName));
			} catch (Exception io) {
				System.out.println("Error while creating the config file" + io);
			}
		}
		return configprop.getProperty(propertyName);
	}

}
