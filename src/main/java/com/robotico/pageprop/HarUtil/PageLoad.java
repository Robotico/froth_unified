package com.robotico.pageprop.HarUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.robotico.base.DriverFactory;
import com.robotico.lib.MainUtil;
import com.robotico.lib.SQLConnectionHelper;
import com.robotico.listener.ExtentTestNGITestListener;

import net.lightbody.bmp.core.har.Har;

public class PageLoad extends DriverFactory {

	public static boolean myElementIsClickable(WebDriver driver, By by) {
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by));
		} catch (WebDriverException ex) {
			return false;
		}
		return true;
	}

	public static void captureNetwork(String pageName) throws IOException {
		HarUtil hu = new HarUtil();
		Har har = DriverFactory.getProxy().getHar();
		String harFileName = MainUtil.sdfyyyyMMddHHmmss.format(new Date()) + "_" + pageName + ".har";

		File outputPath = new File(ExtentTestNGITestListener.harutilsDir + MainUtil.file_separator + harFileName);
		System.out.println(outputPath.getPath());
		FileOutputStream fos = new FileOutputStream(outputPath);
		har.writeTo(fos);
		hu.harParser(DriverFactory.getProxy().getHar());
		String pageData[][] = { { pageName,
				"<span style='color:" + ((hu.totalPageLoadTime_decimal < 10) ? "green" : "red") + ";font-weight:bold'>"
						+ hu.totalPageLoadTime_decimal + " sec" + "</span>",
				"5 sec", hu.totalPageSize_decimal + " KB" } };
		ExtentColor colorPass = ExtentColor.GREEN;
		ExtentColor colorFail = ExtentColor.RED;
		MainUtil.LogInfo(MarkupHelper.createTable(pageData).toString(), pageName);

		if (!MainUtil.db.equalsIgnoreCase("false"))
			SQLConnectionHelper.storePMASintoDB(pageName, hu.totalPageLoadTime_decimal, "5", hu.totalPageSize_decimal,
					hu.totalPageLoadTime_decimal < 10 ? "PASS" : "FAIL", outputPath);

	}

	public static void isjQueryLoaded(WebDriver driver) {
		System.out.println("Waiting for ready state complete");
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				JavascriptExecutor js = (JavascriptExecutor) d;
				String readyState = js.executeScript("return document.readyState").toString();
				System.out.println("Ready State: " + readyState);
				return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
			}
		});
	}
}
