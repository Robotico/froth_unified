package com.robotico.listener;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewStyle;
import com.robotico.lib.MainUtil;

public class ExtentManager {
	public static ExtentSparkReporter sparkReporter;
	public static ExtentReports extent;

	public static ExtentReports getInstance(String jsonFile) {
		if (extent == null) {

			createInstance();
			extent.flush();

			createjson(jsonFile);

		}
		return extent;
	}

	private static void createjson(String jsonFile) {
		try {
			String hostname = System.getProperty("hostname");
			String path = jsonFile;

			JSONObject json = new JSONObject();
			try {
				json.put("id", "-");
				json.put("key", "-");
				json.put("name", System.getProperty("hostname"));
				json.put("result", "Refer Report");
				json.put("os", System.getProperty("os.name").toString());
				json.put("no_of_fails", "Refer Report");
				json.put("verification_date", MainUtil.sdfyyyyMMddhhmmsswithhypen.format(new Date()));
				json.put("report_url", MainUtil.file_separator + "extentreport_" + ExtentTestNGITestListener.executionId
						+ MainUtil.file_separator + ExtentTestNGITestListener.reportHTMLFileName);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try (PrintWriter out = new PrintWriter(new FileWriter(path))) {
				out.write(json.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Create an extent report instance
	public static ExtentReports createInstance() {
		String fileName = getReportPath(ExtentTestNGITestListener.reportDirectoryName);
		sparkReporter = new ExtentSparkReporter(fileName, ViewStyle.SPA);
		sparkReporter.config().setDocumentTitle("Automation Report");
		sparkReporter.config().setReportName("Automation Execution Report");
		sparkReporter.config().setTheme(Theme.DARK);
		sparkReporter.config().setEncoding("UTF_32");
		sparkReporter.setAnalysisStrategy(AnalysisStrategy.SUITE);

		extent = new ExtentReports();
		extent.attachReporter(sparkReporter);
		extent.setSystemInfo("Environment", System.getProperty("env"));
		extent.setSystemInfo("Browser", System.getProperty("browser"));
		extent.setSystemInfo("OS", System.getProperty("os.name").toString());

		/*
		 * BufferedReader br; String fileAsString = null; try { br = new
		 * BufferedReader(new FileReader(path));
		 * 
		 * StringBuilder sb = new StringBuilder(); String line = br.readLine(); while
		 * (line != null) { sb.append(line).append("\n"); line = br.readLine(); }
		 * fileAsString = sb.toString();
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		// extent.setSystemInfo("System Info", fileAsString);

		return extent;

	}

	// Create the report path
	private static String getReportPath(String path) {
		File testDirectory = new File(path);
		if (!testDirectory.exists()) {
			if (testDirectory.mkdir()) {
				System.out.println("Directory: " + path + " is created!");
				return ExtentTestNGITestListener.reportHTMLFileLocation;
			} else {
				System.out.println("Failed to create directory: " + path);
				return System.getProperty("user.dir");
			}
		} else {
			System.out.println("Directory already exists: " + path);
		}
		return ExtentTestNGITestListener.reportHTMLFileLocation;
	}

	public synchronized static String getReportBaseDirectory() {
		return ExtentTestNGITestListener.reportDirectoryName;
	}

}
