package com.robotico.listener;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.robotico.lib.JsonToHashMap;
import com.robotico.lib.MainUtil;
import com.robotico.lib.SQLConnectionHelper;
import com.robotico.powershell.PowerShellBasicCommands;

public class ExtentTestNGITestListener extends TestListenerAdapter {

	private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	public static ExtentReports extent;

	private static String fileSeperator = System.getProperty("file.separator");
	private static String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());

	public static String executionId = System.getProperty("execution_id") == null
			|| System.getProperty("execution_id").equalsIgnoreCase("") ? dateName : System.getProperty("execution_id");
	public static String reportDirectoryName;
	public static String reportHTMLFileName = "robotico" + "_" + executionId + ".html";
	public static String reportHTMLFileLocation;

	public static File passLocation;
	public static File failLocation;
	public static String htmlpassLocation = "./images/Pass";
	public static String htmlfailLocation = "./images/Fail";
	public static String harutilsDir = MainUtil.file_separator + "home" + MainUtil.file_separator + "testopsadmin"
			+ MainUtil.file_separator + "automation" + MainUtil.file_separator + "harfiles" + MainUtil.file_separator
			+ MainUtil.vm_no + MainUtil.file_separator + executionId;

	// EXTENT REPORT
	private static long endTime;
	private static long startTime;

	public ExtentTestNGITestListener() {

		if (System.getProperty("location").equalsIgnoreCase("local")) {
			reportDirectoryName = System.getProperty("user.dir") + fileSeperator + "extentreport_" + executionId;
		} else {
			reportDirectoryName = "S:" + fileSeperator + "summaryreports" + fileSeperator + "extentreport_"
					+ executionId;
		}
		reportHTMLFileLocation = reportDirectoryName + fileSeperator + reportHTMLFileName;

		passLocation = new File(reportDirectoryName + "/images/Pass");
		failLocation = new File(reportDirectoryName + "/images/Fail");

		if (!failLocation.exists()) {
			failLocation.mkdirs();
		}
		if (!passLocation.exists()) {
			passLocation.mkdirs();
		}

		File harDirPath = new File(harutilsDir);
		if (!harDirPath.exists())
			harDirPath.mkdirs();

	}

	/*
	 * private static void setStartTime(long startTime) {
	 * ExtentTestNGITestListener.startTime = startTime; }
	 * 
	 * private static void setEndTime(long endTime) {
	 * ExtentTestNGITestListener.endTime = endTime; }
	 */
	@Override
	public synchronized void onStart(ITestContext iTestContext) {
		// setStartTime(iTestContext.getStartDate().getTime());
		System.out.println("--------------Executing Started for:- " + iTestContext.getName() + "--------------");
		deleteOld15DaysReport();
		MainUtil.scenarioDetails.put("Scenario", iTestContext.getName().replace("\\s+", " "));
		if (!MainUtil.db.equalsIgnoreCase("false"))
			SQLConnectionHelper.setsitedetails();

		String hostname = System.getProperty("hostname");
		String jsonFilename = "S:" + fileSeperator + "summaryreports" + fileSeperator + hostname + "_" + executionId
				+ ".json";
		extent = ExtentManager.getInstance(jsonFilename);
		extent.flush();

		String moduleName = "";
		if (!MainUtil.db.equalsIgnoreCase("false")) {
			SQLConnectionHelper.InsertToExecutionDetails();
			moduleName = SQLConnectionHelper.getModuleName(iTestContext.getName().replace("\\s+", " "));
		}

		String parentTesttext = (moduleName.equalsIgnoreCase("") ? "" : moduleName + "_") + iTestContext.getName();
		ExtentTest parent = extent.createTest(parentTesttext);
		parentTest.set(parent);

		Path dst = Paths.get(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + "SYSTEM_INFO.txt");
		if (!new File(dst.toString()).exists()) {
			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			String path = ps.getSystemInfo();
		}

	}

	@Override
	public synchronized void onFinish(ITestContext iTestContext) {
		// setEndTime(iTestContext.getEndDate().getTime());
		extent.flush();
	}

	@Override
	public synchronized void onTestStart(ITestResult iTestResult) {
		System.out.println("--------------Executing :- " + getSimpleClassName(iTestResult) + "--------------");
		String scenario = MainUtil.scenarioDetails.get("Scenario");
		try {
			if (!(scenario == null || scenario.equalsIgnoreCase(""))) {
				HashMap<String, String> data;
				if (!MainUtil.db.equalsIgnoreCase("false")) {

					data = SQLConnectionHelper.read4mTestDataScenarioTable(scenario, "test_data_scenario_table");
					if (data.size() > 0) {
						MainUtil.scenarioDetails = data;
						MainUtil.scenarioDetails.put("Scenario", scenario);
					}
				} else {
					data = JsonToHashMap.readJsonAndStoreInMap(scenario);
					if (data.size() > 0) {
						MainUtil.scenarioDetails = data;
						MainUtil.scenarioDetails.put("Scenario", scenario);
					}
				}
			} else {
				System.out.println("Scenario is missing ...");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		// System.out.println("Scenario details ----:-" + MainUtil.scenarioDetails==null
		// ?"":MainUtil.scenarioDetails.toString());

		String value = iTestResult.getMethod().getDescription();

		if (value == null || value.equalsIgnoreCase("")) {
			System.out.println(iTestResult.getName() + " has started.");
		} else {
			ExtentTest child = parentTest.get().createNode(value).assignCategory(value);
			test.set(child);
			extent.flush();
		}

		System.out.println("--------------------" + getSimpleClassName(iTestResult) + "---------------------");
	}

	@Override
	public synchronized void onTestSuccess(ITestResult iTestResult) {
		// addExtentLabelToTest(iTestResult);
		extent.flush();
	}

	@Override
	public synchronized void onTestFailure(ITestResult iTestResult) {
		addExtentLabelToTest(iTestResult);
		extent.flush();
	}

	@Override
	public synchronized void onTestSkipped(ITestResult iTestResult) {
		addExtentLabelToTest(iTestResult);
		extent.flush();
	}

	@Override
	public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {

	}

	private synchronized String getSimpleClassName(ITestResult iTestResult) {
		return iTestResult.getTestClass().getRealClass().getSimpleName();
	}

	private synchronized String getSimpleMethodName(ITestResult iTestResult) {
		return iTestResult.getName();
	}

	private synchronized void addExtentLabelToTest(ITestResult iTestResult) {
		if (iTestResult.getStatus() == ITestResult.SUCCESS) {
			getTest().get().pass(MarkupHelper.createLabel("Test Passed", ExtentColor.GREEN));
		} else if (iTestResult.getStatus() == ITestResult.FAILURE) {
			System.out.println(iTestResult.getThrowable());
			getTest().get()
					.fail(MarkupHelper.createLabel(
							"Test Failed " + iTestResult.getName() + " in source: " + iTestResult.getInstanceName(),
							ExtentColor.RED));
		} else {
			getTest().get()
					.skip(MarkupHelper.createLabel(
							"Test Failed " + iTestResult.getName() + " in source: " + iTestResult.getInstanceName(),
							ExtentColor.ORANGE));
		}
	}

	private void deleteOld15DaysReport() {
		try {
			MainUtil.delete4mDirectory(new File(MainUtil.USER_DIR), 15, "extentreport_");
			/*
			 * MainUtil.delete4mDirectory(new File(MainUtil.USER_DIR +
			 * MainUtil.file_separator + "src" + MainUtil.file_separator + "test" +
			 * MainUtil.file_separator + "resources"), 15, "");
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static synchronized void createDescryption(String descryption) {
		getTest().get().info(MarkupHelper.createLabel(descryption, ExtentColor.WHITE));
	}

	public static ThreadLocal<ExtentTest> getParentTest() {
		return parentTest;
	}

	public static ThreadLocal<ExtentTest> getTest() {

		extent.flush();
		return test;

	}

	public static synchronized void createNode(String type) {

		MainUtil.testcaseName = type;
		ExtentTest child = null;
		String jiracode = "";
		if (type.contains(":")) {
			String[] jiracodeno = type.split(":");

			if (jiracodeno[1].contains(",")) {
				String[] jeracodeList = jiracodeno[1].split(",");
				for (int i = 0; i < jeracodeList.length; i++) {
					jiracode += "<a target='_blank' href=\"https://www.google.com/\">" + jeracodeList[i].trim()
							+ "</a>,";
				}
				jiracode = StringUtils.removeEnd(jiracode, ",");
				type = jiracodeno[0] + " : " + jiracode;
				child = parentTest.get().createNode(type);
				for (int i = 0; i < jeracodeList.length; i++) {
					child.assignCategory(jeracodeList[i]);
				}
			} else {
				jiracode += "<a target='_blank' href=\"https://www.google.com/\">" + jiracodeno[1] + "</a>";
				type = jiracodeno[0] + " : " + jiracode;
				child = parentTest.get().createNode(type);
				child.assignCategory(jiracode);
			}
		} else
			child = parentTest.get().createNode(type);

		test.set(child);
	}
}