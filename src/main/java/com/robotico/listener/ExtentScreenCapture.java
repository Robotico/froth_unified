package com.robotico.listener;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.robotico.lib.MainUtil;
import com.robotico.powershell.PowerShellBasicCommands;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class ExtentScreenCapture {
	private static Logger logger = LogManager.getLogger(ExtentScreenCapture.class);

	private ExtentScreenCapture() {
		/* Adding a private constructor */
	}

	/**
	 * This method to capture screenshot for pass
	 *
	 * @param result Pass the driver(Pass/Fail)
	 * @param driver Pass the driver
	 * @return returns mediaModel object
	 */
	public synchronized static MediaEntityModelProvider captureSrceenPass(ITestResult result, RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		String screenshotName = result.getMethod().getMethodName();
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// after execution, you should see a folder "FailedTestsScreenshots"
		// under src folder
		String destination = ExtentTestNGITestListener.passLocation + "/" + screenshotName + dateName + ".png";
		MainUtil.Image_Path = destination;
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(source, finalDestination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlpassLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			logger.error("Error: \n" + e);
		}
		return mediaModel;
	}

	/**
	 * This method to capture screenshot for fail
	 *
	 * @param result Pass the result (pass/fail)
	 * @param driver Pass the driver
	 * @return returns mediaModel object
	 */
	public synchronized static MediaEntityModelProvider captureSrceenFail(ITestResult result, RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		String screenshotName = result.getMethod().getMethodName();
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// after execution, you should see a folder "FailedTestsScreenshots"
		// under src folder
		String destination = ExtentTestNGITestListener.failLocation + "/" + screenshotName + dateName + ".png";
		MainUtil.Image_Path = destination;
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(source, finalDestination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlfailLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			logger.error("Error: \n" + e);
		}
		return mediaModel;
	}

	/**
	 * This method to capture screenshot for pass
	 *
	 * @param urlname Pass the app url name
	 * @param driver  Pass the driver
	 * @return returns mediaModel object
	 */
	public static synchronized MediaEntityModelProvider captureSrceenPass(String urlname, RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		String screenshotName = urlname.equalsIgnoreCase("") ? "AUTO"
				: urlname.trim().replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// after execution, you should see a folder "FailedTestsScreenshots"
		// under src folder
		String destination = ExtentTestNGITestListener.passLocation + "/" + screenshotName + dateName + ".png";
		MainUtil.Image_Path = destination;
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(source, finalDestination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlpassLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static synchronized MediaEntityModelProvider captureSrceenPass(String urlname) {
		MediaEntityModelProvider mediaModel = null;
		urlname = MainUtil.removeSpecialCharcters(urlname);
		String screenshotName = urlname == null || urlname.equalsIgnoreCase("") ? "AUTO"
				: urlname.trim().replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		String destination = ExtentTestNGITestListener.passLocation + "\\" + screenshotName + dateName + ".png";
		try {
			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			ps.captureScreenshot(destination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlpassLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static synchronized MediaEntityModelProvider captureSrceenPassWithPrintCreen(String urlname) {
		MediaEntityModelProvider mediaModel = null;
		urlname = MainUtil.removeSpecialCharcters(urlname);
		String screenshotName = urlname == null || urlname.equalsIgnoreCase("") ? "AUTO"
				: urlname.trim().replaceAll("\\s+", "").trim().toLowerCase();
		String destination = ExtentTestNGITestListener.passLocation + "\\" + screenshotName + ".png";
		try {
			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			ps.captureScreenshotPrintScreen(destination);
			mediaModel = MediaEntityBuilder.createScreenCaptureFromPath(
					ExtentTestNGITestListener.htmlpassLocation + "\\" + screenshotName + ".png").build();
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static synchronized MediaEntityModelProvider captureSrceenFailWithPrintCreen(String urlname) {
		MediaEntityModelProvider mediaModel = null;
		urlname = MainUtil.removeSpecialCharcters(urlname);
		String screenshotName = urlname == null || urlname.equalsIgnoreCase("") ? "AUTO"
				: urlname.trim().replaceAll("\\s+", "").trim().toLowerCase();
		String destination = ExtentTestNGITestListener.failLocation + "\\" + screenshotName + ".png";
		try {
			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			ps.captureScreenshotPrintScreen(destination);
			mediaModel = MediaEntityBuilder.createScreenCaptureFromPath(
					ExtentTestNGITestListener.htmlfailLocation + "/" + screenshotName + ".png").build();
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static synchronized MediaEntityModelProvider captureSrceenFail(String urlname) {
		MediaEntityModelProvider mediaModel = null;
		urlname = MainUtil.removeSpecialCharcters(urlname);
		String screenshotName = urlname == null || urlname.equalsIgnoreCase("") ? "AUTO"
				: urlname.trim().replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		String destination = ExtentTestNGITestListener.failLocation + "\\" + screenshotName + dateName + ".png";
		try {
			PowerShellBasicCommands ps = new PowerShellBasicCommands();
			ps.captureScreenshot(destination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlfailLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mediaModel;
	}

	/**
	 * This method to capture screenshot for fail
	 *
	 * @param urlname Pass the app url name
	 * @param driver  Pass the driver
	 * @return returns mediaModel object
	 */
	public static synchronized MediaEntityModelProvider captureSrceenFail(String urlname, RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		// WebDriver driver = DriverFactory.getDriver();
		String screenshotName = urlname.trim().replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// after execution, you should see a folder "FailedTestsScreenshots"
		// under src folder
		String destination = ExtentTestNGITestListener.failLocation + "/" + screenshotName + dateName + ".png";
		MainUtil.Image_Path = destination;
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(source, finalDestination);
			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlfailLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static String captureScreenshotOfperticualrElement(WebElement element, String imageName,
			RemoteWebDriver driver) {
		String destination = null;
		try {
			MainUtil.scrollToElement(element, driver);
			Thread.sleep(3000);
			File src = element.getScreenshotAs(OutputType.FILE);
			destination = ExtentTestNGITestListener.passLocation + MainUtil.file_separator + imageName + ".png";
			FileHandler.copy(src, new File(destination));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destination;
	}

	public static MediaEntityModelProvider formMediaModel(String filePath) {
		MediaEntityModelProvider mediaModel = null;
		try {
			mediaModel = MediaEntityBuilder.createScreenCaptureFromPath(filePath).build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mediaModel;
	}

	public static void addMultipleImagesToReport(ArrayList<String> fileNames, String imageName) {
		try {

			String ModelsAttached = imageName;
			for (int i = 0; i < fileNames.size(); i++) {
				ModelsAttached += "<img class=\"r-img\" onerror=\"this.style.display=&quot;none&quot;\" data-featherlight='"
						+ ExtentTestNGITestListener.htmlpassLocation + "/" + fileNames.get(i) + "' src='"
						+ ExtentTestNGITestListener.htmlpassLocation + "/" + fileNames.get(i) + "' > ";

			}
			MainUtil.LogPass(ModelsAttached, imageName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static synchronized MediaEntityModelProvider captureFullSrceenShot(String urlname, RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		String screenshotName = urlname.replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());

		Screenshot screenshot = new AShot().takeScreenshot(driver);
		String destination = ExtentTestNGITestListener.passLocation + "/" + screenshotName + dateName + ".png";
		try {
			ImageIO.write(screenshot.getImage(), "jpg", new File(destination));

			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlpassLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	public static MediaEntityModelProvider captureOfperticualrElement(WebElement element, String imageName,
			RemoteWebDriver driver) {
		String destination = null;
		MediaEntityModelProvider mediaModel = null;
		try {
			MainUtil.scrollToElement(element, driver);
			Thread.sleep(3000);
			// File src = element.getScreenshotAs(OutputType.FILE);
			destination = ExtentTestNGITestListener.passLocation + MainUtil.file_separator + imageName + ".png";

			Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
					.takeScreenshot(driver, element);

			ImageIO.write(screenshot.getImage(), "jpg", new File(destination));
			// FileHandler.copy(src, new File(destination));

			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(ExtentTestNGITestListener.htmlpassLocation + "/" + imageName + ".png")
					.build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mediaModel;
	}

	public static synchronized MediaEntityModelProvider captureFullSrceenShotBiggerThnScreenSize(String imageName,
			RemoteWebDriver driver) {
		MediaEntityModelProvider mediaModel = null;
		String screenshotName = imageName.replaceAll(":", "_").replaceAll(" ", "_").trim().toLowerCase() + "_";
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());

		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
				.takeScreenshot(driver);

		String destination = ExtentTestNGITestListener.passLocation + "/" + screenshotName + dateName + ".png";
		try {
			ImageIO.write(screenshot.getImage(), "jpg", new File(destination));

			mediaModel = MediaEntityBuilder
					.createScreenCaptureFromPath(
							ExtentTestNGITestListener.htmlpassLocation + "/" + screenshotName + dateName + ".png")
					.build();
		} catch (IOException e) {
			Reporter.log(e.getMessage());
		}
		return mediaModel;
	}

	synchronized void imageComparision(WebElement logoElemnent, String expectedimg, RemoteWebDriver driver) {
		try {
			Screenshot logoElementScreenshot = new AShot().takeScreenshot(driver, logoElemnent);

			// read the image to compare

			BufferedImage expectedImage;

			expectedImage = ImageIO.read(new File(expectedimg));

			BufferedImage actualImage = logoElementScreenshot.getImage();

			// Create ImageDiffer object and call method makeDiff()

			ImageDiffer imgDiff = new ImageDiffer();
			ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);

			if (diff.hasDiff() == true) {
				System.out.println("Images are same");

			} else {
				System.out.println("Images are different");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static synchronized void AttachTextFile(String fileName) {
		try {
			String path = ExtentTestNGITestListener.htmlpassLocation + File.separator + fileName + ".txt";
			ExtentTestNGITestListener.getTest().get()
					.info("Response- " + "<a href='" + path + "'>" + fileName + ".txt</a>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized void AttachImageFile(String fileName) {
		try {
			String path = ExtentTestNGITestListener.htmlpassLocation + File.separator + fileName + "";
			ExtentTestNGITestListener.getTest().get()
					.info("Response- " + "<a href='" + path + "'>" + fileName + "</a>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}